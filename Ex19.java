/*You are given the following information, but you may prefer to do some research for yourself.

1 Jan 1900 was a Monday.
Thirty days has September,
April, June and November.
All the rest have thirty-one,
Saving February alone,
Which has twenty-eight, rain or shine.
And on leap years, twenty-nine.
A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?*/

import java.util.Scanner;
import java.util.Date;
import java.util.Calendar;
import java.text.SimpleDateFormat;


public class Ex19{
	int numberSundays;

	Ex19(int A, int B) throws Exception{
		numberSundays = 0;
		Date date;
		SimpleDateFormat form = new SimpleDateFormat("dd/MM/yyyy");
		Calendar cal = Calendar.getInstance();
		int day;
		String s;
		
		for(int year = A; year < B+1; year++){
			for(int month = 1; month < 13; month++){
				s = "1/"+month+"/"+year;
				date = form.parse(s);
				cal.setTime(date);
				day = cal.get(Calendar.DAY_OF_WEEK); 
				if(day == 7){
					System.out.print(" " + s);
					numberSundays++;
				}
			}
		}
	}
	
	public static void main(String args[]) throws Exception{
		System.out.print("Find how many Sundays fell on the first of the month from the begin of year A to the end of year B.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter year A: ");
		int A = sc.nextInt();
		System.out.print("\nEnter year B: ");
		int B = sc.nextInt();
		
		Ex19 t = new Ex19(A,B);

		System.out.println("\n\nThe value is "+ t.numberSundays);
		
	}
}