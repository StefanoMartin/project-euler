/*Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.

How many such routes are there through a 20×20 grid?*/

import java.util.Scanner;

public class Ex15{
	long value; 
	
	public long easycombinatorial(int n, int k){
		int kmax = Math.max(k, n-k);
		int kmin = Math.min(k, n-k);
		long i = kmax+1;
		for(int j = kmax+2; j < n+1; j++)
			i *= j;
		for(int j2 = 2; j2 < kmin+1; j2++)
			i /= j2;
		return i;
	}
	
	public long combinatorial(int n, int k){
		if(k == 1 || k == n-1)
			return n;
		else if(k == 0 || k == n)
			return 1;
		else if(n-k < 10)
			return easycombinatorial(n,k);
		else
			return combinatorial(n-1, k-1) + combinatorial(n-1,k);
	}
	
	/*public long Factorial2(int n, int m){
		long i = m+1;
		for(int j = m+2; j < n+1; j++)
			i *= j;
		return i;
	}
	
	public long Factorial(int n){
		long i = 1;
		for(int j = 2; j < n+1; j++)
			i *= j;
		return i;
	}*/
	
	Ex15(int n, int m){
		value = combinatorial(n+m,m);
	}

	
	public static void main(String args[]){
		System.out.print("Find how many such routes are there through an n x m grid\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		System.out.print("\nEnter value m: ");
		int m = sc.nextInt();
		Ex15 t = new Ex15(n,m);
		System.out.println("\nThe value is "+ t.value);
	}
}