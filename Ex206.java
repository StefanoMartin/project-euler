/*
Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0,
where each _ is a single digit.
*/

public class Ex206{	
	long unique;
	
	boolean checkIfOk(long t){
		if(t % 10 != 0)
			return false;
		t /= 100;
		for(int i=9; i>0; i--){
			if(t % 10 != i)
				return false;
			t /= 100;
		}
		return true;
	}
	
	
	Ex206(){
		unique = 0;
		long v = (long)Math.pow(10,9);
		long t;
		while(1==1){
			t = v*v;
			if(checkIfOk(t)){
				unique = v;
				break;
			}
			v += 10;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the unique positive integer whose square has the form 1_2_3_4_5_6_7_8_9_0.\n");		
		Ex206 t = new Ex206();
		System.out.print("\nThe number is "+t.unique+".\n\n");
	}
}