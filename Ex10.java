/*The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.*/

import java.util.Scanner;

public class Ex10{
	int listprime[];
	long sum;
	
	Ex10(int n){
		listprime = new int[n];
		listprime[0] = 2;
		sum = 2;
		int i = 1;
		int t = 2;
		int j;
		while(listprime[i-1] < n){
			t++;
			j = 0;
			while(j < i && t % listprime[j] != 0)
				j++;
			if(j == i){
				listprime[i] = t;
				sum += t;
				i++;
			}
		}
		sum -= listprime[i-1];
	}

	
	public static void main(String args[]){
		System.out.print("Find the sum of all the primes below n.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex10 t = new Ex10(n);
		System.out.println("\nThe value is "+ t.sum);
	}
}