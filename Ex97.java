/*
The first known prime found to exceed one million digits was discovered in 1999, and is a Mersenne prime of the form 2^6972593−1; it contains exactly 2,098,960 digits. 
Subsequently other Mersenne primes, of the form 2p−1, have been found which contain more digits.

However, in 2004 there was found a massive non-Mersenne prime which contains 2,357,207 digits: 28433×2^7830457+1.

Find the last ten digits of this prime number.
*/

import java.util.Scanner;
import java.math.BigInteger;

public class Ex97{	
	String last10cipher;
	
	int compare(int base, int power){
		BigInteger a = new BigInteger(base+"");
		BigInteger myPower = a.pow(power);
		int digitsPow = myPower.toString().length();
		if(digitsPow > power)
			return 2;
		else if(digitsPow == power)
			return 1;
		else
			return 0;
	}
	
	Ex97(int a, int n, int k){
		BigInteger uno = new BigInteger("1");
		BigInteger base = new BigInteger("2");
		BigInteger molt = new BigInteger(a+"");
		BigInteger exp = new BigInteger(n+"");
		BigInteger mod = new BigInteger("10");
		mod = mod.pow(k);
		BigInteger myPower = base.modPow(exp,mod);
		myPower = myPower.multiply(molt);
		myPower = myPower.add(uno);
		myPower = myPower.mod(mod);
		last10cipher = myPower.toString();
	}
	
	public static void main(String[] args){
		System.out.print("Find the last k digits of a x 2^n + 1.\n");	
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value a: ");
		int a = sc.nextInt();
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		System.out.print("\nEnter value n: ");
		int k = sc.nextInt();
		Ex97 t = new Ex97(a,n,k);		
		System.out.print("\nThe last 10 digits of "+a+" x 2^"+n+" + 1 are " + t.last10cipher + ".\n\n");
	}
}