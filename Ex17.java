/*If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.

If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?


NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. 
The use of "and" when writing out numbers is in compliance with British usage. */

import java.util.Scanner;

import java.util.Scanner;

public class Ex17{
	
	String[] n020 = {"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen",	"seventeen", "eighteen", "nineteen"};
	String[] dec  = {"twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
	int sum;

	
	public String Letterization(int n){
		String number = "";
		
		if(n == 0)
			return n020[n];
		
		int a = n / 100000000;
		if(n / 1000000 != 0){
			a = n / (100*1000000);
			if(a != 0){
				number += " " + n020[a];
				number += " hundred";
			}
			n = n - a*100*1000000;
			a = n / (10*1000000);
			if(a >= 2){
				number += " " + dec[a - 2];
				n = n - a*10*1000000;
			}
			a = n / (1*1000000);
			if(a != 0){
				number += " " + n020[a];
				n = n - a*1*1000000;
			}
			number += " million";
		}
		if(n / 1000 != 0){
			a = n / (100*1000);
			if(a != 0){
				number += " " + n020[a];
				number += " hundred";
				n = n - a*100*1000;
			}
			a = n / (10*1000);
			if(a >= 2){
				number += " " + dec[a - 2];
				n = n - a*10*1000;
			}
			a = n / (1*1000);
			if(a != 0){
				number += " " + n020[a];
				n = n - a*1*1000;
			}
			number += " thousand";
		}
		
		a = n / 100;
		if(a != 0){
			number += " " + n020[a];
			number += " hundred";
			n = n - a*100;
			if(n != 0)
				number += " and";
		}
		a = n / 10;
		if(a >= 2){
			number += " " + dec[a - 2];
			n = n - a*10;
		}
		a = n;
		if(a != 0)
			number += " " + n020[a];
		
		return number;
	}
	
	
	
	Ex17(int a, int b){
		sum = 0;
		String c;
		for(int i = a; i < b+1; i++){
			c = Letterization(i);
			System.out.println(c);
			sum += c.replace(" ","").length();
		}
	}

	
	public static void main(String args[]){
		System.out.print("If all the numbers from a to b inclusive were written out in words, find how many letters would be used.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value a: ");
		int a = sc.nextInt();
		System.out.print("\nEnter value b: ");
		int b = sc.nextInt();
		Ex17 t = new Ex17(a,b);
		System.out.println("\nThe value is " + t.sum);
		
	}
}

