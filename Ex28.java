/*
Starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral is formed as follows:

21 22 23 24 25
20  7  8  9 10
19  6  1  2 11
18  5  4  3 12
17 16 15 14 13

It can be verified that the sum of the numbers on the diagonals is 101.

What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed in the same way?
*/

import java.util.Scanner; 

public class Ex28{
	long sum;
	
	public int[][] spiralMatrix(int n){
		int[][] M = new int[n][n];
		int i = 0;
		int left = 1;
		int down = 0;
		int right = 0;
		int up = 0;
		int x = n-1;
		int y = 0;
		
		while(i < n*n){
			//System.out.println(y + "," + x + "," + i);
			M[x][y] = n*n - i;
			i++;
			if(left == 1){
				if(x-1 >= 0 && M[x-1][y] == 0)
					x = x - 1; 
				else{
					left = 0; down = 1;
					y = y + 1;
				}
				continue;
			}
			if(down == 1){
				if(y + 1 < n && M[x][y+1] == 0)
					y = y + 1;
				else{
					down = 0; right = 1;
					x = x + 1;
				}
				continue;
			}
			if(right == 1){
				if(x + 1 < n && M[x+1][y] == 0)
					x = x + 1; 
				else{
					right = 0; up = 1;
					y = y - 1;
				}
				continue;
			}
			if(up == 1){
				if(y - 1 >= 0 && M[x][y - 1] == 0)
					y = y - 1;
				else{
					up = 0; left = 1;
					x = x - 1;
				}
				continue;
			}
		}
	
		return M;
	}
	
	
	Ex28(int n){
		int [][] M = spiralMatrix(n);
		/*for(int i = 0; i < n; i++){
			for(int j = 0; j < n; j++){
				System.out.print(M[i][j] + " ");
			}
			System.out.print("\n");
		}*/
		sum = 0;
		for(int i2 = 0; i2 < n; i2++)
			sum += M[i2][i2] + M[i2][n-1-i2];
		sum -= 1;
	}
	
	public static void main(String[] args){
		System.out.print("Find the the sum of the numbers on the diagonals in a n by n spiral. (n odd)\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex28 t = new Ex28(n);			
		System.out.println("\nThe value is "+ t.sum);
	}
}