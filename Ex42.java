/*
The nth term of the sequence of triangle numbers is given by, tn = ½n(n+1); so the first ten triangle numbers are:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

By converting each letter in a word to a number corresponding to its alphabetical position and adding these values we form a word value. For example, the word value
 for SKY is 19 + 11 + 25 = 55 = t10. If the word value is a triangle number then we shall call the word a triangle word.

Using words.txt (right click and 'Save Link/Target As...'), a 16K text file containing nearly two-thousand common English words, how many are triangle words?
*/

import java.util.Scanner; 
import java.io.*;

public class Ex42{
	int numvalues;
	
	int valueS(String s){
		int n = s.length();
		int score = 0;
		for(int i = 0; i < n; i++){
			score += (int)(s.charAt(i)) - 64;
		}
		return score;
	}
	
	int[] triangleNumbers(){
		int[] M = new int[51];
		for(int i=0; i<51; i++)
			M[i] = i*(i+1)/2;
		return M;
	}
	
	boolean checkIfTriangle(int v, int[] M){
		for(int i=0; i<51; i++){
			if(v == M[i])
				return true;
		}
		return false;
	}
	
	public static void main(String args[]) throws Exception{
		System.out.print("Find how many triangle words are in the file p042_words.\n");
		Scanner sc = new Scanner(new File("p042_words.txt")).useDelimiter(",");
		Ex42 t = new Ex42();
		String s;
		int[] M = t.triangleNumbers();
		int value;
		
		/*for(int i=0; i<51; i++)
			System.out.print(M[i] + " ");*/
		
		t.numvalues = 0;
		while(sc.hasNext()){
			s = sc.next().replace("\"","");
			value = t.valueS(s);
			if(t.checkIfTriangle(value,M))
				t.numvalues++;
		}
		
		System.out.println("\nThe value is "+ t.numvalues);
	}
}