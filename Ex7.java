/*By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?*/

import java.util.Scanner;

public class Ex7{
	int listprime[];
	int nprime;
	
	Ex7(int n){
		listprime = new int[n];
		listprime[0] = 2;
		int i = 1;
		int t = 2;
		int j;
		while(i < n){
			t++;
			j = 0;
			while(j < i && t % listprime[j] != 0)
				j++;
			if(j == i){
				listprime[i] = t;
				i++;
			}
		}
		nprime = listprime[n-1];	
	}

	
	public static void main(String args[]){
		System.out.print("Find the n-th prime number.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex7 t = new Ex7(n);
		System.out.println("\nThe value is "+ t.nprime);
		System.out.print("The other prime before it are:");
		for(int i = 0; i < n; i++)
			System.out.print(" "+ t.listprime[i]);
		System.out.print("\n\n");
	}
}