/*
The prime 41, can be written as the sum of six consecutive primes:

41 = 2 + 3 + 5 + 7 + 11 + 13
This is the longest sum of consecutive primes that adds to a prime below one-hundred.

The longest sum of consecutive primes below one-thousand that adds to a prime, contains 21 terms, and is equal to 953.

Which prime, below one-million, can be written as the sum of the most consecutive primes?
*/

import java.util.Scanner;
import java.util.ArrayList;

public class Ex50{	
	long largest;
	int numPrimelargest;
	
	boolean checkIfPrime(long n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	
	Ex50(int n){
		ArrayList<Long> prime = new ArrayList<Long>();
		largest = 1;
		numPrimelargest = 1;
		int numPrime;
		
		long i = 1, sum;
		first_loop:
		while(i < n){
			i++;
			if(checkIfPrime(i)){
				prime.add(i);
				for(int begin=0; begin<prime.size()-1; begin++){
					for(int end=begin+1; end<=prime.size(); end++){
						sum=0; numPrime = 0;
						for(int j=begin; j<end; j++){
							sum += prime.get(j); numPrime++;
						}
						if(sum > n)
							break first_loop;
						if(numPrimelargest < numPrime && sum < n){
							if(checkIfPrime(sum)){
								largest = sum;
								numPrimelargest = numPrime;
								//System.out.println(largest + " ");
							}
						}
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the prime, below n, can be written as the sum of the most consecutive primes.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex50 t = new Ex50(n);
		System.out.print("\nThe value is "+t.largest+" with "+t.numPrimelargest+ " addends.\n\n");
	}
}