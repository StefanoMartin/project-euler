/*
An irrational decimal fraction is created by concatenating the positive integers:

0.123456789101112131415161718192021...

It can be seen that the 12th digit of the fractional part is 1.

If dn represents the nth digit of the fractional part, find the value of the following expression.

d1 × d10 × d100 × d1000 × d10000 × d100000 × d1000000
*/

import java.util.Scanner; 

public class Ex40{
	long prod;
	
	Ex40(long n){
		prod = 1;
		long k = 1, j = 0, temp;
		int logtemp, a;
		
		//System.out.print("The irrational decimal fraction is 0.");
		for(long i=1; i <= n; i++){
			temp = i;
			logtemp  = (int)Math.log10(temp);
			while(logtemp >= 0){
				a = (int)(temp/(long)Math.pow(10,logtemp));
				temp -= a*(long)Math.pow(10,logtemp);
				logtemp--;
				//System.out.print(a);
				j++;
				if(j == k){
					prod *= a;
					k *= 10;
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("If dn represents the nth digit of the fractional part, find the value of the following expression.\n");
		System.out.print("Productory for i=0 to k of d10^i where k  is the integer part of log_10(t).\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value t: ");
		long n = sc.nextLong();
		Ex40 t = new Ex40(n);			
		System.out.println("\nThe product is "+ t.prod + ".\n\n");
	}
}