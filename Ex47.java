/*
The first two consecutive numbers to have two distinct prime factors are:

14 = 2 × 7
15 = 3 × 5

The first three consecutive numbers to have three distinct prime factors are:

644 = 2² × 7 × 23
645 = 3 × 5 × 43
646 = 2 × 17 × 19.

Find the first four consecutive integers to have four distinct prime factors. What is the first of these numbers?
*/

import java.util.Scanner;
import java.util.ArrayList;

public class Ex47{	
	int k;
	
	boolean checkIfInside(int m, ArrayList<Integer> div){
		for(int i=0; i<div.size(); i++){
			if(m == div.get(i))
				return true;
		}
		return false;
	}
	
	boolean checkMult(int n, int k){
		ArrayList<Integer> divisors; 
		int temp, div, m;
		
		for(int t = 0; t < k; t++){ 
			temp = n+t; div = 2; 
			divisors = new ArrayList<Integer>();
			while(temp != 1){
				m = 1;
				while(temp % div == 0){
					m *= div;
					temp /= div;
				}
				if(m != 1){
					//System.out.print(m + " ");
					if(checkIfInside(m, divisors))
						return false;
					divisors.add(m);
				}
				div++;
			}
			if(divisors.size() % k != 0)
				return false;
		}
		return true;
	}
	
	Ex47(int n){
		k = 2;
		while(1==1){
			//System.out.print("\n" + k + " ---> ");
			if(checkMult(k,n))
				break;
			else
				k++;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the first n consecutive integers to have n distinct prime factors.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex47 t = new Ex47(n);
		System.out.print("The values are");
		for(int j = 0; j < n; j++)
			System.out.print(" " + (t.k + j));
		System.out.print(".\n\n");
	}
}