/*

It was proposed by Christian Goldbach that every odd composite number can be written as the sum of a prime and twice a square.

9 = 7 + 2×12
15 = 7 + 2×22
21 = 3 + 2×32
25 = 7 + 2×32
27 = 19 + 2×22
33 = 31 + 2×12

It turns out that the conjecture was false.

What is the smallest odd composite that cannot be written as the sum of a prime and twice a square?

*/

public class Ex46{	
	int k;
	
	boolean isPrime(long n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	Ex46(){
		k = 7;
		int p, t;
		while(1==1){
			k += 2;
			if(isPrime(k))
				continue;
			t = 1;
			while(2*t*t < k){
				p = k - 2*t*t;
				if(isPrime(p))
					break;
				t++;
			}
			if(2*t*t >= k)
				break;
		}
			
	}
	
	public static void main(String[] args){
		System.out.print("Find the smallest odd composite that cannot be written as the sum of a prime and twice a square.\n");		
		Ex46 t = new Ex46();
		System.out.print("The value is "+t.k+ ".\n\n");
	}
}