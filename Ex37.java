/*
The number 3797 has an interesting property. Being prime itself, it is possible to continuously remove digits from left to right, and remain prime at each stage: 3797, 797, 97, and 7. Similarly we can work from right to left: 3797, 379, 37, and 3.

Find the sum of the only eleven primes that are both truncatable from left to right and right to left.

NOTE: 2, 3, 5, and 7 are not considered to be truncatable primes.
*/

public class Ex37{
	int sum;
	
	boolean checkPrime(long n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	Ex37(){
		int i=0;
		long j=11;
		long tempr, templ;
		int logj;
		while(i < 11){
			if(checkPrime(j)){
				logj  = (int)Math.log10(j);
				tempr = j - (j/(long)Math.pow(10,logj))*(long)Math.pow(10,logj);
				templ = (j - j%10)/10;
				//System.out.println(tempr + " " + templ);
				while(templ != 0){
					if(!checkPrime(tempr))
						break;
					if(!checkPrime(templ))
						break;
					logj  = (int)Math.log10(tempr);
					tempr = tempr - (tempr/(long)Math.pow(10,logj))*(long)Math.pow(10,logj);
					templ = (templ - templ%10)/10;
					//System.out.println(tempr + " " + templ);
				}
				if(templ == 0){
					System.out.print(" " + j);
					i++;
					sum += j;
				}/*else{
					System.out.print("\n " + j + "NO");
				}*/
			}
			j = j+2;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of the only eleven primes that are both truncatable from left to right and right to left.\n");
		Ex37 t = new Ex37();			
		System.out.println("\nThe sum is "+ t.sum + ".\n\n");
	}
}