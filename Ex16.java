/*215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 2^1000?*/

import java.util.Scanner; 
import java.util.ArrayList;

public class Ex16{
	int sum;
	ArrayList<Integer> digits;
	
	public ArrayList<Integer> basisOfNumber(int n, int div){
		ArrayList<Integer> bits = new ArrayList<>();
		int temp;
		while(n != 0){
			temp = n % div;
			n = (n-temp)/div;
			bits.add(temp);
		}
		return bits;
	}
	
	public ArrayList<Integer> MultiplicationByA(ArrayList<Integer> digits, int a){
		int sizeA = digits.size();
		int temp;
		for(int i = 0; i < sizeA; i++){
			temp = digits.get(i);
			digits.set(i,a*temp);
		}
		digits = MoveDecimals(digits);
		return digits;
	}
	
	public ArrayList<Integer> MoveDecimals(ArrayList<Integer> digits){ 
		int sizeA = digits.size();
		int temp;
		int temp2 = 1;
		for(int j = 0; j < sizeA - 1; j++){
			temp	=  digits.get(j) % 10;
			temp2 	=  digits.get(j+1) + (digits.get(j) - temp)/10;
			digits.set(j,temp);
			digits.set(j+1,temp2);
		}
		int k = 0;
		while(temp2 != 0){
			temp = digits.get(sizeA-1+k) % 10;
			temp2 = (digits.get(sizeA-1+k) - temp)/10;
			if(temp2 != 0){
				digits.set(sizeA-1+k, temp);
				digits.add(temp2);
				k++;
			}
		}
		return digits;
	}
	
	public int checkDigits(ArrayList<Integer> f, ArrayList<Integer> g){
		int maxSize = Math.max(f.size(), g.size());
		int k = 1;
		while(k < maxSize)
			k = 2*k;
		return k;
	}
	
	public ArrayList<Integer> SumKaratsuba(ArrayList<Integer> a, ArrayList<Integer> b){
		int k = a.size();
		int temp;
		ArrayList<Integer> c = new ArrayList<>();
		for(int i = 0; i < k; i++){
			temp = a.get(i) + b.get(i);
			c.add(temp);
		}
		return c;
	}
	
	public ArrayList<Integer> SubtractKaratsuba(ArrayList<Integer> a, ArrayList<Integer> b){
		int temp;
		ArrayList<Integer> c = new ArrayList<>();
		for(int i = 0; i < a.size(); i++){
			if(a.get(i) < b.get(i)){
				a.set(i+1,a.get(i+1)-1);
				a.set(i,a.get(i)+10);
			}
			temp = a.get(i) - b.get(i);
			c.add(temp);
		}
		return c;
	}
	
	public ArrayList<Integer> PutTogether(ArrayList<Integer> a, ArrayList<Integer> b, ArrayList<Integer> c, int k){
		ArrayList<Integer> d = new ArrayList<>();
		int k2 = a.size();
		if(k2 == 1){
			d.add(a.get(0));
			d.add(b.get(0));
			d.add(c.get(0));
			return d;
		}
		
		if(k2 % 2 == 0){
			for(int i = 0; i < 2*k2; i++){
				if(i < k2/2)
					d.add(a.get(i));
				else if(i >= k2/2 && i < k2)
					d.add(a.get(i) + b.get(i-k2/2));
				else if(i >= k2 && i < 3*k2/2)
					d.add(b.get(i-k2/2) + c.get(i-k2));
				else if(i >= 3*k2/2 && i < 2*k2)
					d.add(c.get(i-k2));
			}
		}else{
			for(int i = 0; i < 2*k2+1; i++){
				if(i < k2/2+1)
					d.add(a.get(i));
				else if(i >= k2/2+1 && i < k2)
					d.add(a.get(i) + b.get(i-k2/2-1));
				else if(i == k2)
					d.add(b.get(i-k2/2-1));
				else if(i > k2 && i <= 3*k2/2)
					d.add(b.get(i-k2/2-1) + c.get(i-k2-1));
				else if(i > 3*k2/2 && i <= 2*k2)
					d.add(c.get(i-k2-1));
			}
		}

		return d;
	}
	

	
	
	public ArrayList<Integer> Karatsuba(ArrayList<Integer> f, ArrayList<Integer> g, int k){
		ArrayList<Integer> c = new ArrayList<>();
		if(f.size() == 1 && g.size() == 1){
			c.add(f.get(0) * g.get(0));
		}else{
			ArrayList<Integer> f0 = new ArrayList<>();
			ArrayList<Integer> f1 = new ArrayList<>();
			ArrayList<Integer> g0 = new ArrayList<>();
			ArrayList<Integer> g1 = new ArrayList<>();
			int halfk = k/2;
			for(int i = 0; i < halfk; i++){
				f0.add(f.get(i));
				g0.add(g.get(i));
				f1.add(f.get(i+halfk));
				g1.add(f.get(i+halfk));
			}
			ArrayList<Integer> f0g0 = Karatsuba(f0, g0, k/2);
			ArrayList<Integer> f1g1 = Karatsuba(f1, g1, k/2);
			ArrayList<Integer> sumf0f1 = SumKaratsuba(f0, f1);
			ArrayList<Integer> sumg0g1 = SumKaratsuba(g0, g1);
			ArrayList<Integer> f0f1xg0g1 = Karatsuba(sumf0f1, sumg0g1, k/2);
			ArrayList<Integer> temp = new ArrayList<>();
			for(int k1 = 0; k1 < f0g0.size(); k1++)
				temp.add(f0g0.get(k1));
			f0f1xg0g1 = SubtractKaratsuba(f0f1xg0g1,temp);
			for(int k2 = 0; k2 < f1g1.size(); k2++)
				temp.set(k2,f1g1.get(k2));
			f0f1xg0g1 = SubtractKaratsuba(f0f1xg0g1,temp);
			c = PutTogether(f0g0, f0f1xg0g1, f1g1, k/2);

		}

		return c;
	}
	
	Ex16(int a, int n){
		ArrayList<Integer> bits = basisOfNumber(n,2);  // Bits of n
		digits 					= new ArrayList<>(); 
		digits.add(1);
		int k;
		
		int sizeBits = bits.size();
		int thisBit;
		for(int i = 0; i < sizeBits-1; i++){
			thisBit = bits.get(sizeBits-1-i);
			if(thisBit == 1)
				digits = MultiplicationByA(digits,a);
			k = checkDigits(digits,digits);
			while(digits.size() < k)
				digits.add(0);
			digits = Karatsuba(digits, digits, k);
			digits = MoveDecimals(digits);
		}
		if(bits.get(0) == 1)
			digits = MultiplicationByA(digits,a);
		sum = 0;
		for(int i2 = 0; i2 < digits.size(); i2++)
			sum += digits.get(i2);
	}

	
	public static void main(String args[]){
		System.out.print("Find the sum of the digits of the number a^n \n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value a: ");
		int a = sc.nextInt();
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex16 t = new Ex16(a,n);
		System.out.println("\nThe value is "+ t.sum);
		System.out.print("\nThe value "+a+"^"+n+" = ");
		int ok = 0;
		for(int j = 0; j < t.digits.size(); j++)
			if(ok == 1 || t.digits.get(t.digits.size()-1-j) != 0){
				System.out.print(t.digits.get(t.digits.size()-1-j));
				ok = 1;
			}
		System.out.print("\n\n");
	}
}