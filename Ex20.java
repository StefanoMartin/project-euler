/*n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!*/


import java.util.Scanner; 
import java.util.ArrayList;

public class Ex20{
	int sum;
	ArrayList<Integer> digits;
	
	
	Ex20(int n){
		digits = new ArrayList<>(); 
		digits.add(1);
		Ex16 t = new Ex16(1,1);

		for(int a = 2; a < n+1; a++){
			digits = t.MultiplicationByA(digits,a);
			digits = t.MoveDecimals(digits);
		}
		
		for(int i2 = 0; i2 < digits.size(); i2++)
			sum += digits.get(i2);
	}
	
	public static void main(String args[]){
		System.out.print("Find the sum of the digits of the number n! \n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex20 t = new Ex20(n);
		System.out.println("\nThe value is "+ t.sum);
		System.out.print("\nThe value "+n+"! = ");
		int ok = 0;
		for(int j = 0; j < t.digits.size(); j++)
			if(ok == 1 || t.digits.get(t.digits.size()-1-j) != 0){
				System.out.print(t.digits.get(t.digits.size()-1-j));
				ok = 1;
			}
		System.out.print("\n\n");
	}
}