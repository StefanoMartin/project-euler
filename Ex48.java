/*
The series, 1^1 + 2^2 + 3^3 + ... + 10^10 = 10405071317.

Find the last ten digits of the series, 1^1 + 2^2 + 3^3 + ... + 1000^1000.
*/

import java.util.Scanner;
import java.math.BigInteger;

public class Ex48{	
	BigInteger sum;
	
	Ex48(int n){
		sum = new BigInteger(""+0);
		BigInteger iBig;
		for(int i=1; i<=n; i++){
			iBig = new BigInteger(""+i);
			sum = sum.add(iBig.pow(i));
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the value of the series, 1^1 + 2^2 + 3^3 + ... + n^n.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex48 t = new Ex48(n);
		System.out.print("\nThe value is "+t.sum+".\n\n");
	}
}