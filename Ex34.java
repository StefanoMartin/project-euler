/*
145 is a curious number, as 1! + 4! + 5! = 1 + 24 + 120 = 145.

Find the sum of all numbers which are equal to the sum of the factorial of their digits.

Note: as 1! = 1 and 2! = 2 are not sums they are not included.
*/

import java.util.Scanner;

public class Ex34{
	int sum;
	
	int factorial(int n){
		int k = 1;
		for(int i=1; i<=n; i++)
			k *= i;
		return k;
	}
	
	
	boolean checkSum(int n){
		int temp = n;
		int tempsum = 0;
		int a = 0;
		while(temp != 0){
			a = temp % 10;
			//System.out.print(a);
			temp = (temp - a)/10;
			tempsum += factorial(a);
			//System.out.print(factorial(a) + " ");
		}
		//System.out.println(tempsum + " " + n);
		if(tempsum == n)
			return true;
		else
			return false;
	}
	
	
	Ex34(int n){
		sum = 0;
		for(int i=3; i<n; i++){
			if(checkSum(i)){
				System.out.print(i + " ");
				sum += i;
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of all numbers smaller than n which are equal to the sum of the factorial of their digits.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex34 t = new Ex34(n);			
		System.out.println("\nThe value is "+ t.sum + "\n\n");
	}
}