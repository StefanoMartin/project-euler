/*2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?*/

/* NOTE: I know that I can solve by doing 2^4 * 3^2 * 5 * 7 * 11 * 13 * 15 * 17 * 19 but I want to create it brutally. */

import java.util.Scanner;

public class Ex5{
	int smallestnumber;
	
	Ex5(int n){
		smallestnumber = 0;
		int ok = 0;
		int nein;
		int i;
		int j  = 2;
		while(ok == 0){
			nein = 0;
			i	 = 2;
			while(i < n+1 && nein == 0){
				if(j % i != 0){
					j++;
					nein = 1;
				}else{
					i++;
				}
			}
			if(i == n+1){
				ok = 1;
				smallestnumber = j;
			}
		}
	}

	
	public static void main(String args[]){
		System.out.print("Find the smallest positive number that is evenly divisible by all of the numbers from 1 to n.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex5 t = new Ex5(n);
		System.out.println("\nThe value is "+ t.smallestnumber);
	}
}	