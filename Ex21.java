/*Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.

For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.

Evaluate the sum of all the amicable numbers under 10000.*/


import java.util.Scanner; 


public class Ex21{
	int sum;
	
	public int d(int a){
		int d = 1;
		int sqrtTN = (int)Math.sqrt(a);
		if(a % sqrtTN == 0){
			if(sqrtTN * sqrtTN == a)
				d += sqrtTN;
			else
				d += sqrtTN + (a/sqrtTN);
		}
		for(int i = 2; i < sqrtTN; i++){
			if(a % i == 0)
				d += i + (a/i);
		}
		return d;
	}
	
	
	Ex21(int n){
		int M[] = new int[n+1];
		M[0] = 0; M[1] = 0;
		for(int i = 2; i < n+1; i++)
			M[i] = d(i);
		
		sum = 0;
		for(int i = 0; i < n+1; i++){
			if(M[i] < n+1){
				if(i == M[M[i]]){
					if(i != M[i]){
						sum += i + M[i];
						System.out.print(" ("+i+","+M[i]+")");
						M[M[i]] = 2*n;
					}
				}
			}
		}
	}
		
	
	public static void main(String args[]){
		System.out.print("Evaluate the sum of all the amicable numbers under n.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex21 t = new Ex21(n);
		System.out.println("\nThe value is "+ t.sum);
	}
}