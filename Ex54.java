/*
In the card game poker, a hand consists of five cards and are ranked, from lowest to highest, in the following way:

High Card: Highest value card.
One Pair: Two cards of the same value.
Two Pairs: Two different pairs.
Three of a Kind: Three cards of the same value.
Straight: All cards are consecutive values.
Flush: All cards of the same suit.
Full House: Three of a kind and a pair.
Four of a Kind: Four cards of the same value.
Straight Flush: All cards are consecutive values of same suit.
Royal Flush: Ten, Jack, Queen, King, Ace, in same suit.
The cards are valued in the order:
2, 3, 4, 5, 6, 7, 8, 9, 10, Jack, Queen, King, Ace.

If two players have the same ranked hands then the rank made up of the highest value wins; for example, a pair of eights beats a pair of fives (see example 1 below). But if two ranks tie, for example, both players have a pair of queens, then highest cards in each hand are compared (see example 4 below); if the highest cards tie then the next highest cards are compared, and so on.

Consider the following five hands dealt to two players:

Hand	 	Player 1	 	Player 2	 	Winner
1	 	5H 5C 6S 7S KD
Pair of Fives
 	2C 3S 8S 8D TD
Pair of Eights
 	Player 2
2	 	5D 8C 9S JS AC
Highest card Ace
 	2C 5C 7D 8S QH
Highest card Queen
 	Player 1
3	 	2D 9C AS AH AC
Three Aces
 	3D 6D 7D TD QD
Flush with Diamonds
 	Player 2
4	 	4D 6S 9H QH QC
Pair of Queens
Highest card Nine
 	3D 6D 7H QD QS
Pair of Queens
Highest card Seven
 	Player 1
5	 	2H 2D 4C 4D 4S
Full House
With Three Fours
 	3C 3D 3S 9S 9D
Full House
with Three Threes
 	Player 1
The file, poker.txt, contains one-thousand random hands dealt to two players. Each line of the file contains ten cards (separated by a single space): the first five are Player 1's cards and the last five are Player 2's cards. You can assume that all hands are valid (no invalid characters or repeated cards), each player's hand is in no specific order, and in each hand there is a clear winner.

How many hands does Player 1 win?
*/

import java.io.*;
import java.util.Scanner; 
import java.util.Arrays;
import java.util.ArrayList;

class CardSet{
	int[] maxCards = new int[5];
	int value;
	Card[] cards;
	
	boolean couple(Card c[]){
		if(c[0].value == c[1].value){ int maxCards[] = {c[0].value, c[4].value, c[3].value, c[2].value, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; 	}
		if(c[1].value == c[2].value){ int maxCards[] = {c[1].value, c[4].value, c[3].value, c[0].value, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; 	}
		if(c[2].value == c[3].value){ int maxCards[] = {c[2].value, c[4].value, c[1].value, c[0].value, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; 	}
		if(c[3].value == c[4].value){ int maxCards[] = {c[3].value, c[2].value, c[1].value, c[0].value, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; 	}
		return false;
	}
		
	/*boolean couple(Card c[]){
		if(c[0].value == c[1].value	|| c[1].value == c[2].value || 
		c[2].value == c[3].value || c[3].value == c[4].value )
			return true;
		return false;
	}*/
	
	boolean doubleCouple(Card c[]){
		if((c[0].value == c[1].value) && (c[2].value == c[3].value)){ int maxCards[] = {c[2].value, c[0].value, c[4].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		if((c[0].value == c[1].value) && (c[3].value == c[4].value)){ int maxCards[] = {c[3].value, c[0].value, c[2].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		if((c[1].value == c[2].value) && (c[3].value == c[4].value)){ int maxCards[] = {c[3].value, c[1].value, c[0].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		return false;
	}

	
	/*boolean doubleCouple(Card c[]){
		if(	((c[0].value == c[1].value) && (c[2].value == c[3].value)) ||
			((c[0].value == c[1].value) && (c[2].value == c[4].value)) ||
			((c[0].value == c[1].value) && (c[3].value == c[4].value)) ||
			((c[0].value == c[2].value) && (c[3].value == c[4].value)) ||
			((c[1].value == c[2].value) && (c[3].value == c[4].value)) )
			return true;
		return false;
	}*/
	
	boolean triple(Card c[]){
		if(c[0].value == c[1].value && c[0].value == c[2].value){ int maxCards[] = {c[0].value, c[4].value, c[3].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		if(c[1].value == c[2].value && c[1].value == c[3].value){ int maxCards[] = {c[1].value, c[4].value, c[0].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		if(c[2].value == c[3].value && c[2].value == c[4].value){ int maxCards[] = {c[2].value, c[1].value, c[0].value, 0, 0}; System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		return false;
	}
	
	boolean straight(Card c[]){
		if(	c[0].value == c[1].value-1 && c[1].value == c[2].value-1 && c[2].value == c[3].value-1 && c[3].value == c[4].value-1){
			int maxCards[] = {c[4].value, c[3].value, c[2].value, c[1].value, c[0].value};
			System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );
			return true;
		}
		return false;
	}

		boolean flush(Card c[]){
		if( c[0].suit == c[1].suit && c[0].suit == c[2].suit &&	c[0].suit == c[3].suit && c[0].suit == c[4].suit ){
			int maxCards[] = {c[4].value, c[3].value, c[2].value, c[1].value, c[0].value};
			System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );
			return true;
		}
		return false;
	}
	
	boolean fullHouse(Card c[]){
		if(c[0].value == c[1].value && c[0].value == c[2].value && c[3].value == c[4].value){ int maxCards[] = {c[0].value, c[3].value, 0, 0, 0};  System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );return true; }
		if(c[0].value == c[1].value && c[2].value == c[3].value && c[2].value == c[4].value){ int maxCards[] = {c[3].value, c[0].value, 0, 0, 0};  System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );return true; }
		return false;
	}
	
	boolean poker(Card c[]){
		if(c[0].value == c[1].value && c[0].value == c[2].value && c[0].value == c[3].value){ int maxCards[] = {c[0].value, c[4].value, 0, 0, 0};  System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );return true; }
		if(c[1].value == c[2].value && c[1].value == c[3].value && c[1].value == c[4].value){ int maxCards[] = {c[4].value, c[0].value, 0, 0, 0};  System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length );return true; }
		return false;
	}
	
	boolean straightFlush(Card c[]){
		if(flush(c) && straight(c)){ int maxCards[] = {c[4].value, c[3].value, c[2].value, c[1].value, c[0].value};  System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); return true; }
		return false;
	}

	int checkValue(Card[] c){
		int value = 0;
		if(couple(c)) 		value = 1;
		if(doubleCouple(c)) value = 2;
		if(triple(c)) 		value = 3;
		if(straight(c)) 	value = 4;
		if(flush(c)) 		value = 5;
		if(fullHouse(c)) 	value = 6;
		if(poker(c)) 		value = 7;
		if(straightFlush(c)) value = 8;
		return value;
	}
	
	CardSet(Card[] c){
		Arrays.sort(c);
		cards = c;
		int maxCards[] = {c[4].value, c[3].value, c[2].value, c[1].value, c[0].value};
		System.arraycopy( maxCards, 0, this.maxCards, 0, maxCards.length ); 
		value = checkValue(c);
	}
	
	public int compareTo(CardSet c){
		if(this.value > c.value)
			return 1;
		else if(this.value < c.value)
			return -1;
		else{
			for(int i=0; i<c.maxCards.length; i++){
				if(this.maxCards[i] > c.maxCards[i])
					return 1;
				else if(this.maxCards[i] < c.maxCards[i])
					return -1;
			}
			return 0;
		}
	}
}

class Card implements Comparable<Card>{
	int value;
	int suit;
		
	Card(String s){
		char valueChar = s.charAt(0);
		char suitChar  = s.charAt(1);
		switch(valueChar){
			case 'A': value = 14; break;
			case 'T': value = 10; break;
			case 'J': value = 11; break;
			case 'Q': value = 12; break;
			case 'K': value = 13; break;
			default:  value = Character.getNumericValue(valueChar);
		}
		switch(suitChar){
			case 'H': suit = 0; break;
			case 'S': suit = 1; break;
			case 'D': suit = 2; break;
			case 'C': suit = 3; break;
		}
	}
	
	public int compareTo(Card c){
		if(this.value > c.value)
			return 1;
		else if(this.value < c.value)
			return -1;
		else
			return 0;
	}
}

public class Ex54{	
	int hands;
	
	public static void main(String[] args) throws Exception{
		System.out.print("Find how many hands does Player 1 win.\n");	
		Scanner sc = new Scanner(new File("p054_poker.txt"));
		String s;
		Card[] Player1 = new Card[5];
		Card[] Player2 = new Card[5];
		CardSet P1, P2;
		int i = 0, hands = 0, card;
		while(sc.hasNext()){
			s = sc.next();
			System.out.print(s + " ");
			if(i < 5){
				Player1[i] = new Card(s);
			}else{
				Player2[i-5] = new Card(s);
			}
			
			if(i == 9){
				P1 = new CardSet(Player1);
				P2 = new CardSet(Player2);
				if(P1.compareTo(P2) == 1){
					System.out.print("Player1 win");
					hands++;
				}else{
					System.out.print("Player2 win");
				}
				i = 0;
				System.out.print("\n");
			}else
				i++;
		}
		System.out.print("\nPlayer 1 wins "+hands+" hands.\n\n");
	}
}