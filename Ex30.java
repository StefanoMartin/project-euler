/*
Surprisingly there are only three numbers that can be written as the sum of fourth powers of their digits:

1634 = 14 + 64 + 34 + 44
8208 = 84 + 24 + 04 + 84
9474 = 94 + 44 + 74 + 44
As 1 = 14 is not a sum it is not included.

The sum of these numbers is 1634 + 8208 + 9474 = 19316.

Find the sum of all the numbers that can be written as the sum of fifth powers of their digits.
*/

import java.util.Scanner; 
import java.util.ArrayList;

public class Ex30{
	long sum;
	ArrayList<Long> Listnumb = new ArrayList<Long>();
	
	boolean check(long m, int n){
		long realm = m;
		long a = 0;
		int temp;
		while(m != 0){
			temp = (int)(m % 10);
			a += (long)Math.pow(temp, n);
			m = (m - temp)/10;
		}
		if(a == realm){
			Listnumb.add(realm);
			return true;
		}else
			return false;
	}
	
	
	Ex30(int n){
		long temp = (long)Math.pow(10, n);
		sum = 0;
		for(long i = 2; i < temp; i++){
			if(check(i, n))
				sum += i;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of all the numbers that can be written as the sum of n-th powers of their digits.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex30 t = new Ex30(n);			
		System.out.print("These are the values: ");
		for(int i = 0; i < t.Listnumb.size(); i++)
			System.out.print(t.Listnumb.get(i) + " ");
		System.out.println("\nThe value is "+ t.sum + "\n");
	}
}