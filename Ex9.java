/*A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 32 + 42 = 9 + 16 = 25 = 52.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.*/

import java.util.Scanner;

public class Ex9{
	int product;
	int a,b,c;
	
	Ex9(int n){
		product = 0;
		a = 0; b = 0; c = 0;
		int ci;
		first_loop:
		for(int ai = 1; ai < n - 2; ai++){
			for(int bi = 1; bi < n - 1 - ai; bi++){
				ci = n - ai - bi;
				if(ai*ai + bi*bi == ci*ci){
					a = ai;
					b = bi;
					c = ci;
					product = a*b*c;
					break first_loop;
				}
			}
		}		
	}

	
	public static void main(String args[]) throws Exception{
		System.out.print("There exists maybe one Pythagorean triplet for which a + b + c = n. Find the product abc.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex9 t = new Ex9(n);
		System.out.println("\nThe value is "+ t.product);
		System.out.print("\na = "+t.a+", b = "+t.b+", c = "+t.c+"\n\n");
	}
}