/*
A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.

A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.

As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.

Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
*/

import java.util.Scanner;
import java.util.ArrayList;

public class Ex23{
	int sum;
	
	
	
	public boolean IsAbundant(int n){
		Ex21 t = new Ex21(2);
		int m = t.d(n);
		if(m > n)
			return true;
		else
			return false;
	}
	
	Ex23(int n){
		ArrayList<Integer> abundants = new ArrayList<>();
		int[] myint = new int[n+1];
		myint[0] = 0; myint[1] = 1;
		myint[2] = 2; myint[3] = 3;
		for(int i = 4; i <= n; i++){
			myint[i] = i;
			
			if(IsAbundant(i)){
				//System.out.print(i + " ");
				abundants.add(i);
			}
		}
		//System.out.print("Ciao");
		int numbersAbundats = abundants.size();
		int sumabundants;
		for(int i = 0; i < numbersAbundats; i++){
			for(int j = 0; j < numbersAbundats; j++){
				sumabundants = abundants.get(i) + abundants.get(j);
				if(sumabundants < n+1)
					myint[sumabundants] = 0;
			}
		}
		//System.out.print("Ciao");
		sum = 0;		
		for(int i = 0; i <= n; i++){
			if(myint[i] != 0){
				System.out.print(" " + i);
				sum += i;
			}
		}
	}
	
	public static void main(String args[]){
		System.out.print("Find the sum of all the positive integers smaller than n which cannot be written as the sum of two abundant numbers.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex23 t = new Ex23(n);
		System.out.println("\nThe sum is "+ t.sum);
	}
}
	