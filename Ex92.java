/*
A number chain is created by continuously adding the square of the digits in a number to form a new number until it has been seen before.

For example,

44 → 32 → 13 → 10 → 1 → 1
85 → 89 → 145 → 42 → 20 → 4 → 16 → 37 → 58 → 89

Therefore any chain that arrives at 1 or 89 will become stuck in an endless loop. What is most amazing is that EVERY starting number will eventually arrive at 1 or 89.

How many starting numbers below ten million will arrive at 89?
*/


import java.util.Scanner;
import java.util.ArrayList;

public class Ex92{	
	int many;
	
	int checkIf89(int n){
		int m = 0;
		int temp;
		while(n > 0){
			temp = n % 10;
			n = (n - temp)/10;
			m += (temp*temp);
		}
		if(m == 89 || m == 1)
			return m;
		else
			return checkIf89(m);
	}
	
	Ex92(int n){
		many = 0;
		for(int i=1; i<=n; i++){
			if(checkIf89(i) == 89)
				many++;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find hw many starting numbers below n will arrive at 89.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex92 t = new Ex92(n);
		System.out.print("\nThere are "+t.many+" starting numbers.\n\n");
	}
}