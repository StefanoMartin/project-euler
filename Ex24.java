/*
A permutation is an ordered arrangement of objects. For example, 3124 is one possible permutation of the digits 1, 2, 3 and 4. If all of the permutations are listed numerically 
or alphabetically, we call it lexicographic order. The lexicographic permutations of 0, 1 and 2 are:

012   021   102   120   201   210

What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
*/

import java.util.Scanner;


public class Ex24{
	int[] mydigits;
	
	/*public long myNumb(int[] m){
		long mypow = 1;
		long mynumb = 0;
		for(int i = 0; i < 10; i++){
			mynumb += mypow * m[9-i];
			mypow  *= 10;
		}
		return mynumb;
	}*/
	
	public static void swap(int[] m, int i, int j) {
        int temp = m[i];
        m[i] = m[j];
        m[j] = temp;
    }
	
	public static boolean hasNext(int[] m){
		int k; 
        for (k = 10-2; k >= 0; k--)
            if (m[k] < m[k+1]) break;
		if (k == -1) return false;
		
		int j = 10-1;
        while (m[k] > m[j])
            j--;
		
		swap(m, j, k);
		
		
		for (int r = 10-1, s = k+1; r > s; r--, s++)
            swap(m, r, s);

        return true;
	}

	
	Ex24(int n){
		int[] m = {0,1,2,3,4,5,6,7,8,9};
		int position = 2;
		if(n == 1)
			this.mydigits = m;
		else{
			while(hasNext(m) && position < n){
				position++;
			}
			this.mydigits = m;
		}
	}
	
	public static void main(String args[]){
		System.out.print("Find the n-th lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex24 t = new Ex24(n);
		System.out.print("\nThe "+n+"-th lexicographic permutation is ");
		for(int i = 0; i < 10; i++)
			System.out.print(t.mydigits[i]);
		System.out.print("\n\n");
	}
}