/*A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.

Find the largest palindrome made from the product of two 3-digit numbers.*/

import java.util.Scanner;

public class Ex4{
	int largestpalindrome;
	int productA, productB;
	
	Ex4(int n){
		int a = (int)Math.pow(10.0, (double)(n-1));
		int b = (int)Math.pow(10.0, (double)(n));
		largestpalindrome = 0;
		productA = 0;
		productB = 0;
		int mult, tempmult;
		int c[] = new int[2*n];
		int check;
		for(int i = a; i < b; i++){
			for(int j = a; j < b; j++){
				mult = i*j;
				tempmult = mult;
				if(mult > largestpalindrome){
					//System.out.println(mult);
					for(int d = 0; d < 2*n; d++){
						c[d] = tempmult % 10;
						tempmult = (tempmult-c[d])/10;
						//System.out.println(d +","+ c[d]);
					}
					check = 0;
					while(c[check] == c[2*n-1-check] && check < n){
						check += 1;
						//System.out.println(check);
					}
					if(check == n && c[2*n-1] != 0){
						largestpalindrome = mult;
						productA = i;
						productB = j;
						System.out.println("This palindrome " + mult +" is obtained by "+ i +"*"+ j);
					}
				}
			}
		}
	}

	
	public static void main(String args[]){
		System.out.print("Find the largest palindrome made from the product of two n-digit numbers.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex4 t = new Ex4(n);
		System.out.println("\nThe value is "+ t.largestpalindrome);
		System.out.println("\nIt is the result of "+ t.productA + "*" + t.productB);
	}
}	