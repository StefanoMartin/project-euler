/*The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?*/

import java.util.Scanner;

public class Ex3{
	int largestprime;
	
	Ex3(long n){
		long n2 = n;
		int num = 2;
		int pow;
		while(n2 > 1){
			if(n2 % num == 0){
				pow = 0;
				while(n2 % num == 0){
					pow += 1;
					n2 = (long)n2/num;
				}
				System.out.println(num +"^"+ pow + " divides "+n);
			}
			num += 1;
		}
		largestprime = num-1;
	}

	
	public static void main(String args[]){
		System.out.print("What is the largest prime factor of the number n ?\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		long n = sc.nextLong();
		Ex3 t = new Ex3(n);
		System.out.println("\nThe value is "+ t.largestprime);
	}
}	