/*
If we take 47, reverse and add, 47 + 74 = 121, which is palindromic.

Not all numbers produce palindromes so quickly. For example,

349 + 943 = 1292,
1292 + 2921 = 4213
4213 + 3124 = 7337

That is, 349 took three iterations to arrive at a palindrome.

Although no one has proved it yet, it is thought that some numbers, like 196, never produce a palindrome. A number that never forms a palindrome through the 
reverse and add process is called a Lychrel number. Due to the theoretical nature of these numbers, and for the purpose of this problem, we shall assume that
 a number is Lychrel until proven otherwise. In addition you are given that for every number below ten-thousand, it will either (i) become a palindrome in less 
 than fifty iterations, or, (ii) no one, with all the computing power that exists, has managed so far to map it to a palindrome. In fact, 10677 is the first 
 number to be shown to require over fifty iterations before producing a palindrome: 4668731596684224866951378664 (53 iterations, 28-digits).

Surprisingly, there are palindromic numbers that are themselves Lychrel numbers; the first example is 4994.

How many Lychrel numbers are there below ten-thousand?

NOTE: Wording was modified slightly on 24 April 2007 to emphasise the theoretical nature of Lychrel numbers.
*/

import java.util.Scanner;
import java.util.ArrayList;

public class Ex55{	
	int sum;
	
	void printArray(ArrayList<Integer> m){
		int size = m.size();
		for(int i=0; i<size; i++)
			System.out.print(m.get(size-1-i));
	}
	
	ArrayList<Integer> transformInArrayList(int n){
		ArrayList<Integer> m = new ArrayList<Integer>();
		int temp;
		while(n > 0){
			temp = (int)(n % 10);
			m.add(temp);
			n = (n-temp)/10;
		}
		return m;
	}
	
	
	ArrayList<Integer> sumNumPlusInverse(ArrayList<Integer> n){
		ArrayList<Integer> nInv = new ArrayList<Integer>();
		ArrayList<Integer> nSum = new ArrayList<Integer>();
		int size = n.size();
		int temp, temp2;
		for(int i=0; i<size; i++)
			nInv.add(n.get(size-1-i));
		for(int i=0; i<size; i++)
			nSum.add(n.get(i) + nInv.get(i));
		for(int i=0; i<size-1; i++){
			temp = nSum.get(i);
			if(temp > 9){
				temp -= 10;
				temp2 = nSum.get(i+1) + 1;
				nSum.set(i, temp);
				nSum.set(i+1, temp2);
			}
		}
		temp = nSum.get(size-1);
		if(temp > 9){
			temp -= 10;
			nSum.set(size-1, temp);
			nSum.add(1);
		}
		
		/*printArray(n);
		System.out.print(" + ");
		printArray(nInv);
		System.out.print(" = ");
		printArray(nSum);
		System.out.print(" ---> ");*/
		
		return nSum;
	}
	
	/*long inverseNumber(long n){
		long newN = 0;
		int temp;
		while(n > 0){
			newN *= 10;
			temp = (int)(n % 10);
			newN += temp;
			n = (n - temp)/10;
		}
		return newN;
	}*/
	
	boolean checkIfPalindrom(ArrayList<Integer> m){
		int size = m.size();
		for(int i=0; i<size/2; i++){
			if(m.get(i) != m.get(size-1-i))
				return false;
		}
		return true;
	}
	
	boolean Lycherel(ArrayList<Integer> n, int k){
		if(k == 0)
			return false;
		else{
			ArrayList<Integer> sumNN2 = sumNumPlusInverse(n);
			if(checkIfPalindrom(sumNN2)){
				return true;
			}else
				return Lycherel(sumNN2, k-1);
		}
	}
	
	Ex55(int a, int b){
		sum = 0;
		ArrayList<Integer> m;
		for(int n=1; n<b+1; n++){
			m = transformInArrayList(n);
			if(!Lycherel(m,a)){
				sum++;
				System.out.print(n + " ");
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find how many Lychrel numbers are there below N by using k iterations.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value k: ");
		int k = sc.nextInt();
		System.out.print("\nEnter value N: ");
		int n = sc.nextInt();
		Ex55 t = new Ex55(k,n);
		System.out.print("\nThere are "+t.sum+" values.\n\n");
	}
}