/*
If p is the perimeter of a right angle triangle with integral length sides, {a,b,c}, there are exactly three solutions for p = 120.

{20,48,52}, {24,45,51}, {30,40,50}

For which value of p ≤ 1000, is the number of solutions maximised?
*/

import java.util.Scanner; 

public class Ex39{
	int nSolutions, pmax;
	
	Ex39(int n){
		nSolutions = 0;
		pmax = 0;
		int c, ntemp;
		for(int p = 12; p <= n; p++){
			ntemp = 0;
			for(int a = 0; a < n-2; a++){
				for(int b = 0; b < n-2; b++){
					if((a*b) % p == 0){
						c = p/2 - a*b/p;
						if(c > 0 && a*a + b*b == c*c)
							ntemp++;
					}
				}
			}
			if(ntemp > nSolutions){
				nSolutions = ntemp;
				pmax = p;
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find for which value of p <= n, the number of solutions is maximised.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex39 t = new Ex39(n);			
		System.out.println("\nThe value is "+ t.pmax + " with " + t.nSolutions + " solutions.\n\n");
	}
}