/*
It is possible to show that the square root of two can be expressed as an infinite continued fraction.

√ 2 = 1 + 1/(2 + 1/(2 + 1/(2 + ... ))) = 1.414213...

By expanding this for the first four iterations, we get:

1 + 1/2 = 3/2 = 1.5
1 + 1/(2 + 1/2) = 7/5 = 1.4
1 + 1/(2 + 1/(2 + 1/2)) = 17/12 = 1.41666...
1 + 1/(2 + 1/(2 + 1/(2 + 1/2))) = 41/29 = 1.41379...

The next three expansions are 99/70, 239/169, and 577/408, but the eighth expansion, 1393/985, is the first example where the number of digits in the numerator exceeds the number of digits in the denominator.

In the first one-thousand expansions, how many fractions contain a numerator with more digits than denominator?
*/

import java.util.Scanner;
import java.math.BigInteger;

public class Ex57{	
	int many;
	
	boolean compare(BigInteger den, BigInteger num){
		BigInteger tempnum = num.add(den);
		String digitsNum = tempnum.toString();
		String digitsDen = num.toString();
		//System.out.println(digitsNum+"/"+digitsDen);
		if(digitsNum.length() > digitsDen.length())
			return true;
		else
			return false;
	}
	
	Ex57(int n){
		int tempSum;
		many = 0;
		BigInteger num = new BigInteger("5");
		BigInteger den = new BigInteger("2");
		BigInteger temp;
		for(int i=2; i<n+1; i++){
			temp = den.add(num).add(num);
			den = num;
			num = temp;
			if(compare(den, num))
				many++;
		}
	}
	
	public static void main(String[] args){
		System.out.print("In the first N expansions of sqrt(2), find how many fractions contain a numerator with more digits than denominator.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value N: ");
		int n = sc.nextInt();
		Ex57 t = new Ex57(n);
		System.out.print("\nThere are "+t.many+" fractions.\n\n");
	}
}