/*
Take the number 192 and multiply it by each of 1, 2, and 3:

192 × 1 = 192
192 × 2 = 384
192 × 3 = 576
By concatenating each product we get the 1 to 9 pandigital, 192384576. We will call 192384576 the concatenated product of 192 and (1,2,3)

The same can be achieved by starting with 9 and multiplying by 1, 2, 3, 4, and 5, giving the pandigital, 918273645, which is the concatenated product of 9 and (1,2,3,4,5).

What is the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1?
*/

public class Ex38{
	long largest;
	
	Ex38(){
		largest = 918273645;
		long a, tempa;
		int[] m;
		int j, temp;
		
		for(int i = 9182; i < 9877; i++){
			m = new int[9];
			a = i*100000 + 2*i;
			j = 0; tempa = a;
			while(j < 9){
				temp = (int)(tempa % 10);
				if(temp == 0)
					break;
				tempa = (tempa - temp)/10;
				if(m[temp-1] != 0)
					break;
				else
					m[temp-1] = 1;
				j++;
			}
			if(j == 9){
				if(a > largest)
					largest = a;
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the largest 1 to 9 pandigital 9-digit number that can be formed as the concatenated product of an integer with (1,2, ... , n) where n > 1\n");
		Ex38 t = new Ex38();			
		System.out.println("\nThe value is "+ t.largest + ".\n\n");
	}
}