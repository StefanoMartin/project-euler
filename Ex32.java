/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once; for example, the 5-digit number, 15234, is 1 through 5 pandigital.

The product 7254 is unusual, as the identity, 39 × 186 = 7254, containing multiplicand, multiplier, and product is 1 through 9 pandigital.

Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.

HINT: Some products can be obtained in more than one way so be sure to only include it once in your sum.
*/

import java.util.Scanner;
import java.util.ArrayList;

class Pandigital{
	boolean check;
	ArrayList<Integer> m;
	
	Pandigital(int a, long n){
		m = new ArrayList<Integer>();
		m.add(0);
		int temp;
		check = true;
		first_loop:
		while(n != 0){
			temp = (int)(n % 10);
			for(int j=0; j<m.size(); j++){
				if(temp == m.get(j) || temp > a || temp == 0){
					check = false;
					break first_loop;
				}
			}
			m.add(temp);
			n = (n - temp)/10;
		}
	}
	
	Pandigital(int a, long n, ArrayList<Integer> m1){
		m = new ArrayList<Integer>(m1);
		int temp;
		check = true;
		first_loop:
		while(n != 0){
			temp = (int)(n % 10);
			for(int j=0; j<m.size(); j++){
				if(temp == m.get(j) || temp > a || temp == 0){
					check = false;
					break first_loop;
				}
			}
			m.add(temp);
			n = (n - temp)/10;
		}
	}
}
	
	

public class Ex32{
	int sum;
	
	/*ArrayList<Integer> CheckPandigital(int n){
		ArrayList<Integer> m = new ArrayList<Integer>();
		int temp;
		while(n != 0){
			temp = n % 10;
			for(int j=0; j<m.size(); j++){
				if(temp == mAprod.get(j))
					return false;

				if(j == mAprod.size() - 1)
					return false;
			}
			n = (n - temp)/10;
		}
		return true;
	}*/
	
	/*boolean CheckPandigital(int n, ArrayList<Integer> mAprod){
		int temp;
		while(n != 0){
			temp = n % 10;
			for(int j=0; j<mAprod.size(); j++){
				if(temp == mAprod.get(j)){
					mAprod.set(j,0);
					break;
				}
				if(j == mAprod.size() - 1)
					return false;
			}
			n = (n - temp)/10;
		}
		return true;
	}*/
	
	
	/*void printArray(ArrayList<Integer> m){
		for(int j=0; j<m.size(); j++)
			System.out.print(m.get(j) + " ");
		System.out.print("\n");
	}*/
	
	boolean CheckIfEx(long n, ArrayList<Long> m){
		for(int j=0; j<m.size(); j++){
			if(n == m.get(j))
				return false;
		}
		return true;
	}
	
	Ex32(int n){
		long prod;
		long big = 10000;
		sum = 0;
		Pandigital t, t2, t3;
		ArrayList<Long> iA 		= new ArrayList<Long>();
		for(long i=1; i<big; i++){
			t = new Pandigital(n, i);
			if(t.check){
				/*System.out.print("\n" + t.check + " ");
				for(int tt=0; tt<t.m.size(); tt++)
					System.out.print(t.m.get(tt) + " ");*/
				for(long j=1; j<big; j++){
					t2 = new Pandigital(n, j, t.m);
					if(t2.check){
						prod = i * j;
						t3 = new Pandigital(n, prod, t2.m);
						if(t3.check){
							if(t3.m.size() == n+1){
								System.out.println(i + " x " + j + " = " + prod);
								/*if(CheckIfEx(i,iA)){ iA.add(i); sum += i; }
								if(CheckIfEx(j,iA)){ iA.add(j); sum += j; }*/
								if(CheckIfEx(prod,iA)){ iA.add(prod); sum += prod; }
							}
						}
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through n pandigital.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex32 t = new Ex32(n);			
		System.out.println("\nThe value is "+ t.sum + "\n\n");
	}
}