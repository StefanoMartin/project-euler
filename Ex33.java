/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, 
is obtained by cancelling the 9s.

We shall consider fractions like, 30/50 = 3/5, to be trivial examples.

There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator.

If the product of these four fractions is given in its lowest common terms, find the value of the denominator.
*/

public class Ex33{
	int prod;
	
	int[] checkIfFrac(int a, int b){
		int ta = a % 10;
		int tb = b % 10;
		int sa = (a-ta)/10;
		int sb = (b-tb)/10;
		int[] val = new int[2];
		if     (ta == tb & a*sb == b*sa){
			val[0] = sa;	val[1] = sb; }
		else if(ta == sb & a*tb == b*sa){
			val[0] = sa;	val[1] = tb; }
		else if(sa == tb & a*sb == b*ta){
			val[0] = ta;	val[1] = sb; }
		else if(sa == sb & a*tb == b*ta){
			val[0] = ta;	val[1] = tb; }
		else{
			val[0] = 0;		val[1] = 0; }
		return val;
	}
	
	Ex33(){
		int ta, tb;
		int proda = 1, prodb = 1;
		int[] val;
		for(int num=11; num<100; num++){
			for(int den=num+1; den<100; den++){
				if(num % 10 == 0 && den % 10 == 0)
					continue;
				else{
					val = checkIfFrac(num, den);
					if(val[0] != 0){
						System.out.println("\n" + num + "/" + den + " = " + val[0] + "/" + val[1]);
						proda *= val[0];
						prodb *= val[1];
					}
				}
			}
		}
		System.out.println("\nThe value is " + proda + "/" + prodb);
	}
	
	public static void main(String[] args){
		System.out.print("Find the product of the four curios fractions.\n");
		Ex33 t = new Ex33();			
	}
}