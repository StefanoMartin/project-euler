/*
A unit fraction contains 1 in the numerator. The decimal representation of the unit fractions with denominators 2 to 10 are given:

1/2	= 	0.5
1/3	= 	0.(3)
1/4	= 	0.25
1/5	= 	0.2
1/6	= 	0.1(6)
1/7	= 	0.(142857)
1/8	= 	0.125
1/9	= 	0.(1)
1/10	= 	0.1
Where 0.1(6) means 0.166666..., and has a 1-digit recurring cycle. It can be seen that 1/7 has a 6-digit recurring cycle.

Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
*/

import java.util.Scanner; 
import java.util.ArrayList;

public class Ex26{
	
	int denLongest;
	ArrayList<Integer> fractionLongest;
	int[] n0n9longest;

	public ArrayList<Integer> division(int num, int den, int decimals){
		ArrayList<Integer> myRes = new ArrayList<Integer>();
		int i = 0;
		int quo = num/den;
		myRes.add(quo);
		int rem = num - quo*den;
		while(i < decimals && rem != 0){
			num = rem*10;
			quo = num/den;
			myRes.add(quo);
			rem = num - quo*den;
			i++;
		}
		return myRes;
	}
	
	/*public int[] isPeriodic(ArrayList<Integer> num, int decimals, int den){
		int[] n0n9 = {0,0};
		//System.out.print((num.size()-1) + "," + decimals + "\n");
		if((num.size()-1) != decimals)
			return n0n9;
		else{
			long t, value0, value1;
			for(int number0 = 0; number0 < 10; number0++){
				for(int number9 = 1; number9 < 10; number9++){
					value0 = 0;
					for(int i0 = 1; i0 <= number0; i0++)
						value0 += num.get(i0)*Math.pow(10,number0-i0);
					
					value1  = value0*(long)Math.pow(10,number9);
					t = 9*(long)Math.pow(10,number0);
					value1 += num.get(number0+1)*Math.pow(10,number9-1);
					for(int i9 = 1; i9 < number9; i9++){
						t += 9*(long)Math.pow(10,number0+i9);
						value1 += num.get(number0+i9+1)*Math.pow(10,number9-i9-1);
					}
					//System.out.print(value1 + "," + value0 + "," + t + "\n");
					value1 = value1 - value0;
					if(value1 * den == t){
						n0n9[0] = number0; n0n9[1] = number9;
						return n0n9;
					}
				}
			}
		}
		return n0n9;
	}	*/
	
	public int[] isPeriodic(ArrayList<Integer> num, int decimals, int den){
		int[] n0n9 = {0,0};
		//System.out.print((num.size()-1) + "," + decimals + "\n");
		if((num.size()-1) != decimals)
			return n0n9;
		else{
			//System.out.print("Ciao!");
			int ok;
			int times;
			for(int check = 1; check < decimals/2; check++){
				for(int step = 1; step < decimals/10; step++){
					ok = 0;
					times = 1;
					while(ok == 0 && times < 10 && (check + step*times) < decimals){
						if(num.get(check) == num.get(check + step*times))
							times++;
						else
							ok = 1;
					}
					//System.out.print(check +","+ step +","+ times + "\n");
					if(times == 10){
						n0n9[0] = check-1; n0n9[1] = step;
						return n0n9;
					}
				}
			}
		}
		return n0n9;
	}
	
	Ex26(int n){
		int longest = 0;
		denLongest = 0;
		n0n9longest = new int[2];
		fractionLongest = new ArrayList<>();
		
		int[] n0n9;
		ArrayList<Integer> fraction;
		for(int i = 1; i < n; i++){
			fraction 	= division(1,i,10000);
			n0n9 		= isPeriodic(fraction, 10000, i);
			//System.out.print(n0n9[0] +"."+ n0n9[1] + "\n");
			if(n0n9[1] > longest){
				longest = n0n9[1];
				denLongest = i;
				fractionLongest = new ArrayList<>(fraction);
				n0n9longest[0] = n0n9[0];
				n0n9longest[1] = n0n9[1];
			}
		}
	}

	
	public static void main(String args[]){
		System.out.print("For n integer. Find the value of d < n for which 1/d contains the longest recurring cycle in its decimal fraction part.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex26 t = new Ex26(n);			
		System.out.println("\nThe value is "+ t.denLongest);
		System.out.print("The fraction is 1/" + t.denLongest + " and it is equal to: 0.");
		for(int i = 1; i < t.n0n9longest[0]+1; i++)
			System.out.print(t.fractionLongest.get(i));
		System.out.print("(");
		for(int i = t.n0n9longest[0]+1; i < t.n0n9longest[1]+t.n0n9longest[0]+1; i++)
			System.out.print(t.fractionLongest.get(i));
		System.out.print(")\n\n");
	}
}