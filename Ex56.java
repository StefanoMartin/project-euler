/*
A googol (10^100) is a massive number: one followed by one-hundred zeros; 100^100 is almost unimaginably large: one followed by two-hundred zeros. 
Despite their size, the sum of the digits in each number is only 1.

Considering natural numbers of the form, ab, where a, b < 100, what is the maximum digital sum?
*/

import java.util.Scanner;
import java.math.BigInteger;

public class Ex56{	
	int bigSum;
	int bigA;
	int bigB;
	
	int sumDigits(BigInteger n){
		String digits = n.toString();
		int sum = 0;

		for(int i = 0; i < digits.length(); i++) {
			int digit = (int) (digits.charAt(i) - '0');
			sum = sum + digit;
		}
		return sum;
	}
	
	Ex56(int k1, int k2){
		int tempSum;
		bigSum = 0; bigA = 0; bigB = 0;
		BigInteger iBig, powBig;
		for(int a=2; a< k1; a++){
			iBig = new BigInteger(""+a);
			for(int b=2; b< k1; b++){
				powBig = iBig.pow(b);
				tempSum = sumDigits(powBig);
				if(tempSum > bigSum){
					bigSum = tempSum;
					bigA = a;
					bigB = b;
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Considering natural numbers of the form, a^b, where a < k_1, b < k_2, what is the maximum digital sum?\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value k_1: ");
		int k1 = sc.nextInt();
		System.out.print("\nEnter value k_2: ");
		int k2 = sc.nextInt();
		Ex56 t = new Ex56(k1,k2);
		System.out.print("\nThe power with maximum digital sum is "+t.bigA+"^"+t.bigB+" with digital sum equal to " +t.bigSum + ".\n\n");
	}
}