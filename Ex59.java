/*
Each character on a computer is assigned a unique code and the preferred standard is ASCII (American Standard Code for Information Interchange). 
For example, uppercase A = 65, asterisk (*) = 42, and lowercase k = 107.

A modern encryption method is to take a text file, convert the bytes to ASCII, then XOR each byte with a given value, taken from a secret key. 
The advantage with the XOR function is that using the same encryption key on the cipher text, restores the plain text; for example, 65 XOR 42 = 107, then 107 XOR 42 = 65.

For unbreakable encryption, the key is the same length as the plain text message, and the key is made up of random bytes. 
The user would keep the encrypted message and the encryption key in different locations, and without both "halves", it is impossible to decrypt the message.

Unfortunately, this method is impractical for most users, so the modified method is to use a password as a key. 
If the password is shorter than the message, which is likely, the key is repeated cyclically throughout the message. 
The balance for this method is using a sufficiently long password key for security, but short enough to be memorable.

Your task has been made easy, as the encryption key consists of three lower case characters. 
Using cipher.txt (right click and 'Save Link/Target As...'), a file containing the encrypted ASCII codes, and the 
knowledge that the plain text must contain common English words, decrypt the message and find the sum of the ASCII values in the original text.
*/

import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;

public class Ex59{
	
	
	
	/*Ex59(ArrayList<Integer> c){
		int sum;
		int k, t;
		for(int i1=97; i1<123; i1++){
			for(int i2=97; i2<123; i2++){
				for(int i3=97; i3<123; i3++){
					sum = 0;
					String s = "";
					//System.out.print("\n");
					t=0;
					for(int i=0; i<c.size(); i++){
						t++;
						k = c.get(i);
						switch(i%3){
							case 0: k = k^i1; break;
							case 1: k = k^i2; break;
							case 2: k = k^i3; break;
						}
						if((k >= 0 && k <= 31) || (k >= 33 && k <= 38) || (k >= 45 && k <= 64) || (k >= 91 && k <= 96) || (k >= 123 && k <= 127)){
							//System.out.print(k+"-"+Character.toString((char)k)+" ");
							break;							
						}
						s += Character.toString((char)k); sum += k; 
						//System.out.print(s);
					}
					//System.out.print(t + " ");
					if(t > 20)
						System.out.print(i1 + " " + i2 + " " + i3 + " " + s+" "+sum+"\n\n");
				}
			}
		}
	} 
	<--- By using it I found that the values to decrpyt are 
	i1 = 103, i2 = 111, i3 = 100
	I.e. "god".

	*/
	
	Ex59(ArrayList<Integer> c){
		int sum = 0;
		String s = "";
		int i1 = 103;
		int i2 = 111;
		int i3 = 100;
		int k;
		for(int i=0; i<c.size(); i++){
			k = c.get(i);
				switch(i%3){
					case 0: k = k^i1; break;
					case 1: k = k^i2; break;
					case 2: k = k^i3; break;
				}
			s += Character.toString((char)k); sum += k; 
		}
		System.out.print("The message is: " +s+"\nThe sum is "+sum+".\n\n");
	}
	
	public static void main(String args[]) throws FileNotFoundException{
		System.out.print("Decipher the code\n");
		Scanner sc = new Scanner(System.in);
		ArrayList<Integer> cybertext = new ArrayList<Integer>();
		int a, stop;
		Scanner scFile = new Scanner(new File("p059_cipher.txt")).useDelimiter(",");
		
		stop = 0;
		while(scFile.hasNextInt()){
			a = scFile.nextInt();
			//System.out.print(a+ " ");
			cybertext.add(a);
		}
		
		Ex59 t = new Ex59(cybertext);

	}
}