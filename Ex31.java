/*
In England the currency is made up of pound, £, and pence, p, and there are eight coins in general circulation:

1p, 2p, 5p, 10p, 20p, 50p, £1 (100p) and £2 (200p).
It is possible to make £2 in the following way:

1×£1 + 1×50p + 2×20p + 1×5p + 1×2p + 3×1p
How many different ways can £2 be made using any number of coins?
*/

import java.util.Scanner;

public class Ex31{
	int many;
	
	Ex31(int n){
		many = 0;
		int n1, n2, n3, n4, n5, n6, n7;
		
		for(int i200 = 0; i200 <= n; i200++){
			n1 = n - 200*i200;
			for(int i100 = 0; i100 <= n1; i100++){
				n2 = n1 - 100*i100;
				for(int i50 = 0; i50 <= n2; i50++){
					n3 = n2 - 50*i50;
					for(int i20 = 0; i20 <= n3; i20++){
						n4 = n3 - 20*i20;
						for(int i10 = 0; i10 <= n4; i10++){
							n5 = n4 - 10*i10;
							//System.out.println(i10);
							for(int i5 = 0; i5 <= n5; i5++){
								//System.out.println(i5);
								n6 = n5 - 5*i5;
								//System.out.println(n6);
								for(int i2 = 0; i2 <= n6/2; i2++){
									n7 = n6 - 2*i2;
									many++;
									System.out.println(i200+" x 2P + "+i100+" x 1P + "+i50+" x 50p + "+i20+ " x 20p + "+i10+" x 10p + "+i5+" x 5p + "+i2+" x 2p + "+n7+" x 1p");
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find how many different ways can n be made using any number of coins.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex31 t = new Ex31(n);			
		System.out.println("\nThe value is "+ t.many + "\n\n");
	}
}
	
	