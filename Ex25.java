/*
The Fibonacci sequence is defined by the recurrence relation:

Fn = Fn−1 + Fn−2, where F1 = 1 and F2 = 1.
Hence the first 12 terms will be:

F1 = 1
F2 = 1
F3 = 2
F4 = 3
F5 = 5
F6 = 8
F7 = 13
F8 = 21
F9 = 34
F10 = 55
F11 = 89
F12 = 144
The 12th term, F12, is the first term to contain three digits.

What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
*/

import java.util.Scanner; 
import java.util.ArrayList;

public class Ex25{
	ArrayList<Integer> myFib;
	int index;
	
	public ArrayList<Integer> MoveDecimals(ArrayList<Integer> digits){ 
		int sizeA = digits.size();
		int temp;
		int temp2 = 1;
		for(int j = 0; j < sizeA - 1; j++){
			temp	=  digits.get(j) % 10;
			temp2 	=  digits.get(j+1) + (digits.get(j) - temp)/10;
			digits.set(j,temp);
			digits.set(j+1,temp2);
		}
		int k = 0;
		while(temp2 != 0){
			temp = digits.get(sizeA-1+k) % 10;
			temp2 = (digits.get(sizeA-1+k) - temp)/10;
			if(temp2 != 0){
				digits.set(sizeA-1+k, temp);
				digits.add(temp2);
				k++;
			}
		}
		return digits;
	}
	
	public ArrayList<Integer> SumDecimals(ArrayList<Integer> a, ArrayList<Integer> b){
		int sizeA = a.size();
		int sizeB = b.size();
		int bigSize = Math.max(sizeA, sizeB);
		int temp;
		//System.out.println(a); System.out.println(b);
		ArrayList<Integer> c = new ArrayList<>();
		while(a.size() < bigSize)
			a.add(0);
		while(b.size() < bigSize)
			b.add(0);
		for(int i = 0; i < bigSize; i++){
			temp = a.get(i) + b.get(i);
			c.add(temp);
			//System.out.println(c);
		} 
		c = MoveDecimals(c);
		return c;
	}
	
	
	Ex25(int n){
		ArrayList<Integer> imin = new ArrayList<>();
		ArrayList<Integer> imax = new ArrayList<>();
		ArrayList<Integer> inew;
		imin.add(1); imax.add(2);
		index = 2;
		//System.out.print("1 2 ");
		while(imax.size() < n){
			index++;
			inew = SumDecimals(imin,imax);
			/*for(int j = 0; j < inew.size(); j++)
				System.out.print(inew.get(inew.size()-1-j));*/
			//System.out.print(" ");
			imin = new ArrayList<>(imax);
			imax = new ArrayList<>(inew);
		}
		myFib = new ArrayList<>(imax);
	}

	
	public static void main(String args[]){
		System.out.print("Find the index of the first term in the Fibonacci sequence to contain n digits.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex25 t = new Ex25(n);
		System.out.println("\nThe value is "+ (t.index+1));
		System.out.print("\nThe first Fibonacci term with more than "+n+" digits is ");
		int ok = 0;
		for(int j = 0; j < t.myFib.size(); j++)
			if(ok == 1 || t.myFib.get(t.myFib.size()-1-j) != 0){
				System.out.print(t.myFib.get(t.myFib.size()-1-j));
				ok = 1;
			}
		System.out.print("\n");
	}
}	