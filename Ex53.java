/*
There are exactly ten ways of selecting three from five, 12345:

123, 124, 125, 134, 135, 145, 234, 235, 245, and 345

In combinatorics, we use the notation, 5C3 = 10.

In general,

nCr =	
n!
r!(n−r)!
,where r ≤ n, n! = n×(n−1)×...×3×2×1, and 0! = 1.
It is not until n = 23, that a value exceeds one-million: 23C10 = 1144066.

How many, not necessarily distinct, values of  nCr, for 1 ≤ n ≤ 100, are greater than one-million?
*/

import java.util.Scanner;

public class Ex53{	
	int sum;
	
	double comb(int n, int k){
		double output = 1;
		for(int i=k+1; i<=n; i++){
			output *= i;
			output /= (i-k);
		}
		return output;
	}
	
	
	Ex53(int a, int b){
		sum = 0;
		for(int n=1; n<a+1; n++){
			if(n%2 == 1){
				for(int k=2; k<=(n-1)/2; k++){
					if(comb(n,k) > b)
						sum += 2;
				}
			}else{
				for(int k=2; k<=(n-2)/2; k++){
					if(comb(n,k) > b)
						sum += 2;
				}
				if(comb(n,n/2) > b)
					sum++;
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find how many, not necessarily distinct, values of  nCr, for 1 <= n <= k, are greater than N?\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value k: ");
		int k = sc.nextInt();
		System.out.print("\nEnter value N: ");
		int n = sc.nextInt();
		Ex53 t = new Ex53(k,n);
		System.out.print("\nThere are "+t.sum+" values.\n\n");
	}
}