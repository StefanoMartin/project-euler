/*
We shall say that an n-digit number is pandigital if it makes use of all the digits 1 to n exactly once. For example, 2143 is a 4-digit pandigital and is also prime.

What is the largest n-digit pandigital prime that exists?
*/

public class Ex41{
	long largest;
	
	boolean checkPrime(long n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	boolean checkPandigital(long n){
		int a, logn = (int)Math.log10(n);
		//System.out.println(logn);
		int[] digits = new int[logn+1];
		while(n > 0){
			a = (int)(n % 10);
			//System.out.println(a);
			if(a == 0 || a > logn+1) 
				return false;
			if(digits[a-1] == 1)
				return false;
			digits[a-1] = 1;
			n = (n - a)/10;
		}
		return true;
	}
	
	Ex41(){
		largest = 0;
		int i=7654321;
		//System.out.println(checkPandigital(2143));
		while(i > 0){
			if(checkPandigital(i) && checkPrime(i)){
				largest = i;
				break;
			}
			i -= 2;
		}	
	}
	
	public static void main(String[] args){
		System.out.print("Find the largest n-digit pandigital prime that exists.\n");
		Ex41 t = new Ex41();			
		System.out.println("\nThe value is "+ t.largest + ".\n\n");
	}
}