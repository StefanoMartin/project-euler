/*
Pentagonal numbers are generated by the formula, Pn=n(3n−1)/2. The first ten pentagonal numbers are:

1, 5, 12, 22, 35, 51, 70, 92, 117, 145, ...

It can be seen that P4 + P7 = 22 + 70 = 92 = P8. However, their difference, 70 − 22 = 48, is not pentagonal.

Find the pair of pentagonal numbers, Pj and Pk, for which their sum and difference are pentagonal and D = |Pk − Pj| is minimised; what is the value of D?
*/

import java.util.Arrays;

public class Ex44{
	int n = 10000;
	int D;
	int[] M = PentagonalNumbers();
	

	
	int[] PentagonalNumbers(){
		int[] M = new int[n];
		for(int i=0; i<n; i++)
			M[i] = i*(3*i-1)/2;
		return M;
	}
	
	boolean checkIfInside(int m){
		for(int i=0; i<n; i++){
			if(M[i] == m)
				return true;
			if(M[i] > m)
				return false;
		}
		return false;
	}
	
	Ex44(){
		D = 0;
		int sum, diff;
		this_loop:
		for(int k=1; k<n; k++){
			for(int i=k+1; i<n; i++){
				sum = M[i] + M[i-k];
				if(checkIfInside(sum)){
					//System.out.println(i + " , " + (i-k));
					diff = M[i] - M[i-k];
					if(checkIfInside(diff)){
						//System.out.print(diff + " ");
						D = diff;
						System.out.println(M[i] + " , " + M[i-k]);
						break this_loop;
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the pair of pentagonal numbers, Pj and Pk, for which their sum and difference are pentagonal and D = |Pk − Pj| is minimised; find the value of D.\n");
		Ex44 t = new Ex44();			
		System.out.println("\nThe value is "+ t.D + ".\n\n");
	}
}