/*
The decimal number, 585 = 1001001001 (binary), is palindromic in both bases.

Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.

(Please note that the palindromic number, in either base, may not include leading zeros.)
*/

import java.util.Scanner;
import java.util.ArrayList;

public class Ex36{
	int sum;
	
	ArrayList<Integer> CreateBinary(long n){
		ArrayList<Integer> m = new ArrayList<Integer>();
		int temp;
		while(n != 0){
			temp = (int)(n % 2);
			m.add(temp);
			n = (n - temp)/2;
		}
		return m;
	}
	
	boolean CheckBinaryPol(ArrayList<Integer> m){
		
		int size = m.size();
		int k = size%2==0 ? size/2 : (size-1)/2;
		for(int i = 0; i <= k; i++){
			if(m.get(i) != m.get(size-1-i))
				return false;
		}
		return true;
	}
	
	
	
	Ex36(long n){
		ArrayList<Integer> m;
		long temp;
		int temp2, temp3, j, logj, t;
		sum = 0;
		
		j = 1;
		while(1==1){
			logj = (int)Math.log10(j);
			temp = (long)(j*Math.pow(10,logj));
			temp2 = (j - j % 10)/10;
			t = 1;
			while(temp2 != 0){
				temp3 = (int)temp2 % 10;
				temp  += (long)(temp3*Math.pow(10,logj-t));
				temp2 = (temp2 - temp3) / 10;
				t++;
			}
			if(temp > n)
				break;
			
			m = CreateBinary(temp);
			if(CheckBinaryPol(m)){
				sum += temp;
				System.out.print("\n" + temp + "\t -- ");
				for(int i=0; i<m.size(); i++)
					System.out.print(m.get(i));
			}
			
			j++;
		}
		
		j = 1;
		while(2==2){
			logj = (int)Math.log10(j);
			temp = (long)(j*Math.pow(10,logj+1));
			t = 1;
			temp2 = j;
			while(temp2 != 0){
				temp3 = (int)temp2 % 10;
				temp  += (long)(temp3*Math.pow(10,logj+1-t));
				temp2 = (temp2 - temp3) / 10;
				t++;
			}
			if(temp > n)
				break;
			
			m = CreateBinary(temp);
			if(CheckBinaryPol(m)){
				sum += temp;
				System.out.print("\n" + temp + "\t -- ");
				for(int i=0; i<m.size(); i++)
					System.out.print(m.get(i));
			}
			
			j++;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of all numbers, less than n, which are palindromic in base 10 and base 2.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		long n = sc.nextLong();
		Ex36 t = new Ex36(n);			
		System.out.println("\nThe sum is "+ t.sum +".\n\n");
	}
}