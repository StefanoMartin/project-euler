/*The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.*/

import java.util.Scanner;

public class Ex14{
	long[] listCollatz;
	long longest;
	int iterations; 
	
	public long Collatz(long n){
		if(n % 2 == 0)
			return n/2;
		else
			return 3*n+1;
	}
	
	Ex14(int n){
		longest = 0;
		iterations = 0;
		long value;
		int tempIterations;
		
		for(int i = 1; i < n; i++){
			tempIterations = 1;
			value = i;
			while(value != 1){
				value = Collatz(value);
				tempIterations++;
			}
			if(tempIterations > iterations){
				longest = i;
				iterations = tempIterations;
			}
		}
		
		listCollatz = new long[iterations];
		tempIterations = 1;
		value = longest;
		listCollatz[0] = value;
		while(value != 1){
			value = Collatz(value);
			listCollatz[tempIterations] = value;
			tempIterations++;
		}
	}

	
	public static void main(String args[]){
		System.out.print("Find the starting number that, under n, produces the longest chain.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex14 t = new Ex14(n);
		System.out.println("\nThe value is "+ t.longest);
		System.out.println("\nIt needs "+ t.iterations + " iterations.");
		System.out.print("\n Its iterations is are ");
		for(int j = 0; j < t.iterations-1; j++)
			System.out.print(t.listCollatz[j] + " -> ");
		System.out.print(t.listCollatz[t.iterations-1] + "\n");
	}
}