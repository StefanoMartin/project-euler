/*
The number, 197, is called a circular prime because all rotations of the digits: 197, 971, and 719, are themselves prime.

There are thirteen such primes below 100: 2, 3, 5, 7, 11, 13, 17, 31, 37, 71, 73, 79, and 97.

How many circular primes are there below one million?
*/

import java.util.Scanner;

public class Ex35{
	int howmany;
	
	boolean checkPrime(long n){
		if(n % 2 == 0)
			return false;
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	boolean checkCircular(long n){
		int temp, k, i, t;
		
		if(n == 3 || n == 5 || n == 7)
			return true;
		if(n == 9)
			return false;
		
		k = 1; i = 0;
		while(k < n){
			k *= 10;
			i++;
		}
		k /= 10;
		i--;
		
		t = 0;
		while(t <= i){
			if(!checkPrime(n))
				return false;
			temp = (int)(n % 10);
			n 	 = (n - temp)/10;
			n	 += k*temp;
			t++;
		}
		return true;	
	}
	
	Ex35(long n){
		howmany = 1;
		if(n < 2)
			howmany = 0;
		else if(n == 2){
			System.out.print(" 2 ");
			howmany = 1;
		}else{
			System.out.print(" 2 ");
			for(long i=3; i<=n; i += 2){
				//System.out.print( i + " ");
				if(checkCircular(i)){
					System.out.print( i + " ");
					howmany++;
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find how many circular primes are there below n.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		long n = sc.nextLong();
		Ex35 t = new Ex35(n);			
		System.out.println("\nThere are "+ t.howmany + " circular values.\n\n");
	}
}