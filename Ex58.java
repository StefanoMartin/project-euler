/*
Starting with 1 and spiralling anticlockwise in the following way, a square spiral with side length 7 is formed.

37 36 35 34 33 32 31
38 17 16 15 14 13 30
39 18  5  4  3 12 29
40 19  6  1  2 11 28
41 20  7  8  9 10 27
42 21 22 23 24 25 26
43 44 45 46 47 48 49

It is interesting to note that the odd squares lie along the bottom right diagonal, but what is more interesting is that 8 out of 
the 13 numbers lying along both diagonals are prime; that is, a ratio of 8/13 ≈ 62%.

If one complete new layer is wrapped around the spiral above, a square spiral with side length 9 will be formed. If this process is continued,
 what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below 10%?
*/

import java.util.Scanner;


public class Ex58{	
	int side;
	double perc;
	
	boolean isPrime(int n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	

	Ex58(double n){
		perc = 100.0;
		side = 1;
		int allCorner = 0;
		int allPrimeCorner = 0;
		int position = 1;
		int step = 2;
		int k;
		while(perc > n){
			k = 0;
			while(k < 4){
				position += step;
				k++;
				allCorner++;
				if(isPrime(position))
					allPrimeCorner++;
			}
			step += 2;
			side += 2;
			perc = (allPrimeCorner*1.0) / (allCorner*1.0) * 100;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find what is the side length of the square spiral for which the ratio of primes along both diagonals first falls below N%.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value N: ");
		double n = sc.nextDouble();
		Ex58 t = new Ex58(n);
		System.out.print("\nThe side length is "+t.side+" with percentage: "+ t.perc + "%.\n\n");
	}
}