/*Work out the first ten digits of the sum of the following one-hundred 50-digit numbers.

See Ex13longnumbers.txt file. */

import java.util.Scanner;
import java.io.*;

public class Ex13{
	int[] AM;
	
	Ex13(int M[][]){
		int temp, temp2;
		AM = new int[100];
		
		
		for(int j = 0; j < 50; j++){
			for(int i = 0; i < 100; i++){
				AM[j] += M[i][j];
				//System.out.print(M[i][j]);
			}
			//System.out.print(AM[j] + "\n");
			temp	=  AM[j] % 10;
			AM[j+1] += (AM[j] - temp)/10;
			AM[j] 	=  temp;
		}
		
		int j2 = 0;
		while(AM[50+j2] != 0){
			//System.out.println(AM[50+j2]);
			temp	=  AM[50+j2] % 10;
			AM[50+j2+1] += (AM[50+j2] - temp)/10;
			AM[50+j2] = temp;
			j2++;
		}
	}

	
	public static void main(String args[]) throws Exception{
		System.out.print("Find the first n digits of the sum of the following one-hundred 50-digit numbers.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		
		FileInputStream fin = new FileInputStream("Ex13longnumbers.txt");
		int M[][] = new int[100][50];
		int temp, j;
		
		for(int i = 0; i < 100; i++){
			j = 0;
			while(j < 50){
				temp = Character.getNumericValue((char)fin.read());
				if(temp != -1){
					M[i][49-j] = temp;
					j++;
				}
			}
		}
		
		/*for(int i = 0; i < 100; i++){
			for(int j2 = 0; j2 < 50; j2++){
				System.out.print(M[i][j2]);
			}
			System.out.print("\n");
		}*/
		
		Ex13 t = new Ex13(M);
		System.out.print("\nThe value is ");
		int j2 = 0;
		int i2 = 99;
		int ok = 0;
		while(j2 < n){
			if(t.AM[i2] != 0 || ok == 1){
				ok = 1;
				System.out.print(t.AM[i2]);
				j2++;
			}
			i2--;
		}
		System.out.print("\n");
	}
}