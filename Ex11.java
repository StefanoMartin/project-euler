/*In the 20×20 grid below, four numbers along a diagonal line have been marked in red.

08 02 22 97 38 15 00 40 00 75 04 05 07 78 52 12 50 77 91 08
49 49 99 40 17 81 18 57 60 87 17 40 98 43 69 48 04 56 62 00
81 49 31 73 55 79 14 29 93 71 40 67 53 88 30 03 49 13 36 65
52 70 95 23 04 60 11 42 69 24 68 56 01 32 56 71 37 02 36 91
22 31 16 71 51 67 63 89 41 92 36 54 22 40 40 28 66 33 13 80
24 47 32 60 99 03 45 02 44 75 33 53 78 36 84 20 35 17 12 50
32 98 81 28 64 23 67 10 26 38 40 67 59 54 70 66 18 38 64 70
67 26 20 68 02 62 12 20 95 63 94 39 63 08 40 91 66 49 94 21
24 55 58 05 66 73 99 26 97 17 78 78 96 83 14 88 34 89 63 72
21 36 23 09 75 00 76 44 20 45 35 14 00 61 33 97 34 31 33 95
78 17 53 28 22 75 31 67 15 94 03 80 04 62 16 14 09 53 56 92
16 39 05 42 96 35 31 47 55 58 88 24 00 17 54 24 36 29 85 57
86 56 00 48 35 71 89 07 05 44 44 37 44 60 21 58 51 54 17 58
19 80 81 68 05 94 47 69 28 73 92 13 86 52 17 77 04 89 55 40
04 52 08 83 97 35 99 16 07 97 57 32 16 26 26 79 33 27 98 66
88 36 68 87 57 62 20 72 03 46 33 67 46 55 12 32 63 93 53 69
04 42 16 73 38 25 39 11 24 94 72 18 08 46 29 32 40 62 76 36
20 69 36 41 72 30 23 88 34 62 99 69 82 67 59 85 74 04 36 16
20 73 35 29 78 31 90 01 74 31 49 71 48 86 81 16 23 57 05 54
01 70 54 71 83 51 54 69 16 92 33 48 61 43 52 01 89 19 67 48

The product of these numbers is 26 × 63 × 78 × 14 = 1788696.

What is the greatest product of four adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?*/

import java.util.Scanner;
import java.io.*;

public class Ex11{
	long product;
	int[] values;
	
	Ex11(int n, int[][] M){
		product = 0;
		long tempproduct;
		values = new int[n];
		
		/*Check horizontals*/
		for(int r1 = 0; r1 < 20; r1++){
			for(int c1 = 0; c1 <= 20-n; c1++){
				tempproduct = 1;
				for(int t1 = 0; t1 < n; t1++)
					tempproduct *= M[r1][c1+t1];
				if(tempproduct > product){
					product = tempproduct;
					for(int i1 = 0; i1 < n; i1++)
						values[i1] = M[r1][c1+i1];
				}
			}
		}
		
		/*Check verticals*/
		for(int c2 = 0; c2 < 20; c2++){
			for(int r2 = 0; r2 <= 20-n; r2++){
				tempproduct = 1;
				for(int t2 = 0; t2 < n; t2++)
					tempproduct *= M[r2+t2][c2];
				if(tempproduct > product){
					product = tempproduct;
					for(int i2 = 0; i2 < n; i2++)
						values[i2] = M[r2+i2][c2];
				}
			}
		}
		
		/*Check diagonals NW - SE*/
		for(int r3 = 0; r3 <= 20-n; r3++){
			for(int c3 = 0; c3 <= 20-n; c3++){
				tempproduct = 1;
				for(int t3 = 0; t3 < n; t3++)
					tempproduct *= M[r3+t3][c3+t3];
				if(tempproduct > product){
					product = tempproduct;
					for(int i3 = 0; i3 < n; i3++)
						values[i3] = M[r3+i3][c3+i3];
				}
			}
		}
		
		/*Check diagonals NE - SW*/
		for(int r4 = 0; r4 <= 20-n; r4++){
			for(int c4 = n-1; c4 < 20; c4++){
				tempproduct = 1;
				for(int t4 = 0; t4 < n; t4++)
					tempproduct *= M[r4+t4][c4-t4];
				if(tempproduct > product){
					product = tempproduct;
					for(int i4 = 0; i4 < n; i4++)
						values[i4] = M[r4+i4][c4-i4];
				}
			}
		}
	}

	
	public static void main(String args[]) throws FileNotFoundException{
		System.out.print("Find the greatest product of n adjacent numbers in the same direction (up, down, left, right, or diagonally) in the 20×20 grid?\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		
		Scanner scFile = new Scanner(new File("Ex11table.txt"));
		int M[][] = new int[20][20];
		
		for(int i = 0; i < 20; i++){
			for(int j = 0; j < 20; j++){
				M[i][j] = scFile.nextInt();
			}
		}
		
		/*for(int i = 0; i < 20; i++){
			for(int j = 0; j < 20; j++){
				System.out.print(M[i][j] + " ");
			}
			System.out.print("\n");
		}*/
		
		Ex11 t = new Ex11(n, M);
		System.out.println("\nThe value is "+ t.product);
		System.out.print("\nThe multipliers are ");
		for(int j = 0; j < n-1; j++)
			System.out.print(t.values[j] + " x ");
		System.out.print(t.values[n-1] + "\n");
	}
}