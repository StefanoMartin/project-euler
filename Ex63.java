/*
The 5-digit number, 16807=7^5, is also a fifth power. Similarly, the 9-digit number, 134217728=8^9, is a ninth power.

How many n-digit positive integers exist which are also an nth power?
*/

import java.util.Scanner;
import java.math.BigInteger;

public class Ex63{	
	int many;
	
	int compare(int base, int power){
		BigInteger a = new BigInteger(base+"");
		BigInteger myPower = a.pow(power);
		int digitsPow = myPower.toString().length();
		if(digitsPow > power)
			return 2;
		else if(digitsPow == power)
			return 1;
		else
			return 0;
	}
	
	Ex63(){
		
		int base = 1;
		int pow = 1;
		int result;
		many = 0;
		
		while(1 == 1){
			result = compare(base,pow);
			if(result == 0 && base == 9)
				break;
			if(result == 0)
				base++;
			else if(result == 1){
				many++;
				System.out.print(base+"^"+pow+" ");
				base++;
			}else{
				base = 1;
				pow++;
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find how many n-digit positive integers exist which are also an nth power.\n");		
		Ex63 t = new Ex63();
		System.out.print("\nThere are "+t.many+" positive integers.\n\n");
	}
}