/*
The arithmetic sequence, 1487, 4817, 8147, in which each of the terms increases by 3330, is unusual in two ways: (i) each of the three terms are prime, and, 
(ii) each of the 4-digit numbers are permutations of one another.

There are no arithmetic sequences made up of three 1-, 2-, or 3-digit primes, exhibiting this property, but there is one other 4-digit increasing sequence.

What 12-digit number do you form by concatenating the three terms in this sequence?
*/

import java.util.Scanner;

public class Ex49{	
	
	boolean checkIfInside(int m, int m2, int m3){
		int[][] A = new int[3][4];
		int temp, j = 0;
		while(m != 0){
			A[0][j] = m % 10;
			m = (m-A[0][j])/10;
			j++;
		}
		while(m2 != 0){
			temp = m2 % 10;
			for(int i=0; i<4; i++){
				if(temp == A[0][i] && A[1][i] == 0){
					A[1][i] = 1;
					break;
				}
				if(i == 3)
					return false;
			}
			m2 = (m2-temp)/10;
		}
		while(m3 != 0){
			temp = m3 % 10;
			for(int i=0; i<4; i++){
				if(temp == A[0][i] && A[2][i] == 0){
					A[2][i] = 1;
					break;
				}
				if(i == 3)
					return false;
			}
			m3 = (m3-temp)/10;
		}
		return true;
	}
	
	boolean checkIfPrime(int n){
		if(n == 1)
			return false;
		if(n == 2 || n == 3)
			return true;
		if(n % 2 == 0)
			return false;
		
		else{
			long sqrtN = (long)Math.sqrt(n);
			for(long i=3; i<=Math.sqrt(n)+2; i+=2){
				if(n % i == 0)
					return false;
			}
		}
		return true;
	}
	
	
	Ex49(int n){
		int t2, t3;
		int[] A = {n*1+n*10+n*100, n*1+n*10+n*1000, n*1+n*100+n*1000, n*10+n*100+n*1000, n+n*10+n*100+n*1000};
		for(int t1=2; t1<9999-2*A[4]; t1++){
			if(checkIfPrime(t1)){
				for(int i=0; i<5; i++){
					t2 = t1+A[i];
					t3 = t2+A[i];
					if(checkIfPrime(t2) && checkIfPrime(t3)){
						//System.out.print("Prime values are "+t1+" "+t2+" "+t3+".\n\n");
						if(checkIfInside(t1, t2, t3))
							System.out.print("One value is "+t1+t2+t3+" with "+A[i]+".\n\n");
					}
				}
			}
		}
	}
	
	public static void main(String[] args){
		System.out.print("Find the 12-digit number do you form by concatenating the three terms in this sequence.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex49 t = new Ex49(n);
	}
}

