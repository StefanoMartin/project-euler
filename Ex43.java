/*
The number, 1406357289, is a 0 to 9 pandigital number because it is made up of each of the digits 0 to 9 in some order, but it also has a rather interesting 
sub-string divisibility property.

Let d1 be the 1st digit, d2 be the 2nd digit, and so on. In this way, we note the following:

d2d3d4=406 is divisible by 2
d3d4d5=063 is divisible by 3
d4d5d6=635 is divisible by 5
d5d6d7=357 is divisible by 7
d6d7d8=572 is divisible by 11
d7d8d9=728 is divisible by 13
d8d9d10=289 is divisible by 17
Find the sum of all 0 to 9 pandigital numbers with this property.
*/

import java.util.Arrays;

public class Ex43{
	long sum;

	/*boolean checkPandigital(long n){
		int a, logn = (int)Math.log10(n);
		int[] digits = new int[10];
		while(n > 0){
			a = (int)(n % 10);
			if(digits[a] == 1)
				return false;
			digits[a] = 1;
			n = (n - a)/10;
		}
		return true;
	}*/
	
	
	public void permutations(int[] n, int[] Nr, int idx) {
		if(idx == n.length){  //stop condition for the recursion [base clause]
			//System.out.println(Arrays.toString(n) + " " + Arrays.toString(Nr));
			if(checkProperty(n)){
				long k = 1;
				for(int j=0; j<10; j++){
					sum += n[9-j]*k;
					k *= 10;
					System.out.print(n[j]);
				}
				System.out.print(" ");
			}
			return;
		}
		for(int i = 0; i <= 9; i++) { 
			if(Nr[i] == 0){
				n[idx] = i;
				Nr[i]  = 1;
				permutations(n, Nr, idx+1); //recursive invokation, for next elements
				Nr[i] = 0;
			}
			
		}
	}
	
	
	boolean checkProperty(int[] Nr){
		int k = 1;
		if(Nr[0] == 0)
			return false;
		int d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 17 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 13 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 11 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 7 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 5 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 3 != 0)
			return false;
		d = Nr[8-k]*100+Nr[9-k]*10+Nr[10-k]; k++;
		if(d % 2 != 0)
			return false;
		return true;
	}
	
	Ex43(){
		sum = 0;
		int[] n = new int[10];
		int Nr[] = new int[10];
		permutations(n, Nr, 0);
	}
	
	public static void main(String[] args){
		System.out.print("Find the sum of all 0 to 9 pandigital numbers with the interesting property.\n");
		Ex43 t = new Ex43();			
		System.out.println("\nThe value is "+ t.sum + ".\n\n");
	}
}