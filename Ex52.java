/*
It can be seen that the number, 125874, and its double, 251748, contain exactly the same digits, but in a different order.

Find the smallest positive integer, x, such that 2x, 3x, 4x, 5x, and 6x, contain the same digits.
*/

import java.util.Scanner;

public class Ex52{	
	long smallest;
	
	int[] createArray(long n){
		int[] arr = new int[10];
		int temp;
		for(int i=0; i<10; i++)
			arr[i] = 0;
		while(n > 0){
			temp = (int)(n % 10);
			arr[temp]++;
			n = (n-temp)/10;
		}
		return arr;
	}
	
	boolean checkIfSameDigits(int[] arr1, int[] arr2){
		for(int i=0; i<10; i++){
			if(arr1[i] != arr2[i])
				return false;
		}
		return true;
	}
	
	
	Ex51(int n){
		smallest = 125870;
		long i, i2;
		int[] arr1, arr2;
		while(1 == 1){
			i2 = smallest;
			arr1 = createArray(smallest);
			/*System.out.print("\n"+smallest+" ");
			for(int k=0; k<10; k++)
				System.out.print(arr1[k]+" ");*/
			i = 1;
			while(i < n){
				i2 += smallest;
				arr2 = createArray(i2);
				if(!checkIfSameDigits(arr1,arr2)){
					//System.out.print(" " + i2);
					break;
				}
				//System.out.print(" " + i2 + " <---------");
				i++;
			}
			if(i == n)
				break;
			smallest++;
		}
	}
	
	public static void main(String[] args){
		System.out.print("Given N, Find the smallest positive integer, x, such that 2x, 3x, ... Nx, contain the same digits.\n");		
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value N: ");
		int n = sc.nextInt();
		Ex52 t = new Ex52(n);
		System.out.print("\nThe value is "+t.smallest+".\n\n");
	}
}