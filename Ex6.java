/*The sum of the squares of the first ten natural numbers is,

12 + 22 + ... + 102 = 385
The square of the sum of the first ten natural numbers is,

(1 + 2 + ... + 10)2 = 552 = 3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.*/

import java.util.Scanner;

public class Ex6{
	int difference, sumsquares, squaresum;
	
	Ex6(int n){
		sumsquares = n*(n+1)*(2*n+1)/6;
		squaresum  = n*(n+1)*n*(n+1)/4;
		difference = squaresum - sumsquares;
	}

	
	public static void main(String args[]){
		System.out.print("Find the difference between the sum of the squares of the first n natural numbers and the square of the sum.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		Ex6 t = new Ex6(n);
		System.out.println("\nThe value is "+ t.difference);
		System.out.println("The sum of squares is "+ t.sumsquares);
		System.out.println("The square of the sum is "+ t.squaresum + "\n");
	}
}	