/*
If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.*/

import java.util.Scanner;

public class Ex1{
	int total;
	
	Ex1(int n, int m1, int m2){
		int i = +1;
		total = 0;
		while(i < n-1){
			i += 1;
			if(i % m1 == 0 || i % m2 == 0)
				total += i;
		}
	}
	
	public static void main(String args[]){
		System.out.print("Find the sum of all the multiples of m1 or m2 below n.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value n: ");
		int n = sc.nextInt();
		System.out.print("\nEnter value m1: ");
		int m1 = sc.nextInt();
		System.out.print("\nEnter value m2: ");
		int m2 = sc.nextInt();
		Ex1 t = new Ex1(n,m1,m2);
		System.out.println("\nThe value is "+ t.total);
	}
}		