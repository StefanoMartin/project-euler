/*
Euler discovered the remarkable quadratic formula:

n² + n + 41

It turns out that the formula will produce 40 primes for the consecutive values n = 0 to 39. However, when n = 40, 402 + 40 + 41 = 40(40 + 1) + 41 is divisible by 41, and certainly when n = 41, 41² + 41 + 41 is clearly divisible by 41.

The incredible formula  n² − 79n + 1601 was discovered, which produces 80 primes for the consecutive values n = 0 to 79. The product of the coefficients, −79 and 1601, is −126479.

Considering quadratics of the form:

n² + an + b, where |a| < 1000 and |b| < 1000

where |n| is the modulus/absolute value of n
e.g. |11| = 11 and |−4| = 4
Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.
*/

import java.util.Scanner; 

public class Ex27{
	long product;
	int Ma, Mb;
	
	public boolean isPrime(int n){
		n = Math.abs(n);
		int sqrtN = (int)Math.sqrt(n);
		if(n == 0 || n == 1)
			return false;
		for(int i = 2; i < sqrtN + 1; i++){
			if(n % i == 0)
				return false;
		}
		return true;
	}
	
	public int maybeQuadratic(int a, int b, int n){
		return n*n + a*n + b;
	}

	public boolean isQuadraticPrime(int a, int b){
		int absA = Math.abs(a);
		for(int n = 0; n < absA; n++){
			if(!(isPrime(maybeQuadratic(a,b,n))))
				return false;
		}
		return true;
	}
	
	Ex27(int maxA, int maxB){
		Ma = 0;
		Mb = 0;
		product = 1;
		int MaxPrime = 0;
		//System.out.println(isQuadraticPrime(-986,-860));
		
		for(int a = -maxA + 1; a < maxA; a++){
			for(int b = -maxB + 1; b < maxB; b++){
				if(a != 0 && isQuadraticPrime(a,b)){
					//System.out.println("n^2 + ("+a+")n + ("+b+") generates quadratic primes");
					if(Math.abs(a)-1 > MaxPrime){
						MaxPrime = Math.abs(a)-1;
						Ma = a; Mb = b;
					}
				}
			}
		}
		product = Ma*Mb;
	}
	
	public static void main(String[] args){
		System.out.print("Considering quadratics of the form: n^2 + an + b, where |a| < A and |b| < B\n");
		System.out.print("Find the product of the coefficients, a and b, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n = 0.\n");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nEnter value A: ");
		int A = sc.nextInt();
		System.out.print("\nEnter value B: ");
		int B = sc.nextInt();
		Ex27 t = new Ex27(A,B);			
		System.out.println("\nThe value is "+ t.product);
		System.out.println("\nThe quadratic expression that produces the maximum number of primes is n^2 + ("+t.Ma+")n + ("+t.Mb+").\n");
	}
}
