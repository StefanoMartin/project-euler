/*
Using names.txt (right click and 'Save Link/Target As...'), a 46K text file containing over five-thousand first names, begin by sorting it into alphabetical order. 
Then working out the alphabetical value for each name, multiply this value by its alphabetical position in the list to obtain a name score.

For example, when the list is sorted into alphabetical order, COLIN, which is worth 3 + 15 + 12 + 9 + 14 = 53, is the 938th name in the list. So, COLIN would obtain a 
score of 938 × 53 = 49714.

What is the total of all the name scores in the file?
*/

import java.util.Scanner; 
import java.io.*;
import java.util.Arrays;

public class Ex22{
	int[] score;
	String[] names;
	int sum;
	
	public int Score(String s, int j){
		int n = s.length();
		int score = 0;
		for(int i = 0; i < n; i++){
			score += (int)(s.charAt(i)) - 64;
		}
		return score*j;
	}
	
	
	public static void main(String args[]) throws Exception{
		System.out.print("Find the total of all the name scores in the file.\n");
		Scanner sc = new Scanner(new File("p022_names.txt")).useDelimiter(",");
		
		int n = 0;
		while(sc.hasNext()){
			sc.next();
			n++;
		}
			
		Ex22 t = new Ex22();
			
		t.names = new String[n];
		sc = new Scanner(new File("p022_names.txt")).useDelimiter(",");
		int i = 0;
		while(sc.hasNext()){
			//System.out.print(t.names[i] + " ");
			t.names[i] = sc.next().replace("\"","");
			i++;
		}
		Arrays.sort(t.names);
		
		t.sum = 0;
		for(int i2 = 0; i2 < n; i2++)
			t.sum += t.Score(t.names[i2], i2+1);

		
		System.out.println("\nThe value is "+ t.sum);
	}
}