Project Euler is a serie of challenges that are solvable only by programming. Here the link: https://projecteuler.net/

If you want to solve Project Euler exercises, don't use this repository. The main objective of Project Euler exercises is to have interesting challenges to test your own ability to code in one language. If I solve for you, it would not be anymore an interesting challenge for you.

-------------------------------------------------------

All the programs in this repository are written by Stefano Martin.
The used program is Java and the exercises are solved to obtain the answer quickly.

All the programs are more general than the request proposed by the project Euler's exercises.
For example if the exercise ask:
"What is the index of the first term in the Fibonacci sequence to contain 1000 digits?"
The program is able to solve this question:
"What is the index of the first term in the Fibonacci sequence to contain n digits?" (where n is a positive integer bigger than 2).

-------------------------------------------------------

The code of each exercise is available in a java file.

To compile the code use:
javac Ex(numberexercise).java

To use the code write:
java Ex(numberexercise)